import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-estudios',
  templateUrl: './estudios.component.html',
  styleUrls: ['./estudios.component.scss']
})
export class EstudiosComponent implements OnInit {

  public estudiosPaciente = {
    estudios:''
  };
  @Output() recetaMedica = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  estudios(){
    this.recetaMedica.emit(this.estudiosPaciente)
  }

}
