import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NotaMedicaComponent } from './nota-medica/nota-medica.component';
import { RecetaComponent } from './receta/receta.component';
import { EstudiosComponent } from './estudios/estudios.component';
import { AntecendentesHeredoFamiliaresComponent } from './historia-clinica/antecendentes-heredo-familiares/antecendentes-heredo-familiares.component';
import { AntecendentesPersonalesNoPatologicosComponent } from './historia-clinica/antecendentes-personales-no-patologicos/antecendentes-personales-no-patologicos.component';
import { AntecendentesPersonalesPatologicosComponent } from './historia-clinica/antecendentes-personales-patologicos/antecendentes-personales-patologicos.component';
import { AntecendentesGinecobstetricosComponent } from './historia-clinica/antecendentes-ginecobstetricos/antecendentes-ginecobstetricos.component';


@NgModule({
  declarations: [
    NotaMedicaComponent,
    RecetaComponent,
    EstudiosComponent,
    AntecendentesHeredoFamiliaresComponent,
    AntecendentesPersonalesNoPatologicosComponent,
    AntecendentesPersonalesPatologicosComponent,
    AntecendentesGinecobstetricosComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
  ],
  exports:[
    NotaMedicaComponent,
    RecetaComponent,
    EstudiosComponent,
    AntecendentesHeredoFamiliaresComponent,
    AntecendentesPersonalesNoPatologicosComponent,
    AntecendentesPersonalesPatologicosComponent,
    AntecendentesGinecobstetricosComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
   ],
})
export class ComponentsModule { }
