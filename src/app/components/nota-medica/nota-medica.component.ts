import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import { Cie10Service } from 'src/app/services/cie10/cie10.service';

@Component({
  selector: 'app-nota-medica',
  templateUrl: './nota-medica.component.html',
  styleUrls: ['./nota-medica.component.scss']
})
export class NotaMedicaComponent implements OnInit {

  public resp:any[] = [];
  public cie10:any[]=[];
  public encontrados:any [] = [];
  @Output() notaMedico = new EventEmitter();
  @ViewChild('cie') cie!: ElementRef;
  @ViewChild('diag') diag!: ElementRef;


  constructor(private _cie10:Cie10Service) { }

  ngOnInit(): void {
    this.obtenerCIE10();
  }

  obtenerCIE10(){
    this.resp = this._cie10.obtenerCie10();
  }

  //funcion para buscar cie10
  buscarCie10(event:any){
    this.encontrados = [];
    if (event.target.value == '') {
      this.encontrados = [];
    } else if(event.target.value.length > 3){
      this.resp.forEach((element) => {
        if (element.NOMBRE.toLowerCase().includes(event.target.value.toLowerCase())) {
          this.encontrados.push(element)
        }
      })
    }
  }
  //fin de la funcion

  selected(event:any){
    this.cie.nativeElement.value='';
    this.encontrados = [];
    this.cie10.push(event);
    this.formulario(this.cie10);
  }

  eliminar(i:any){
    this.cie10.forEach((item, index) => {
      if (index  === i ) {
        this.cie10.splice( index, 1 )
      }
    });
    this.formulario(this.cie10);
  }

  formulario(value:any){
    this.notaMedico.emit(value);
  }

}
