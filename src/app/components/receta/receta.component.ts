import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { paciente } from '../../interfaces/paciente'
import { medico } from '../../interfaces/medico'
import { NgForm } from '@angular/forms';
import { AngularFirestore} from '@angular/fire/firestore';
import { buscar } from 'src/app/functions/buscarMedicamentos';

@Component({
  selector: 'app-receta',
  templateUrl: './receta.component.html',
  styleUrls: ['./receta.component.scss']
})
export class RecetaComponent implements OnInit {

  @Input() paciente:paciente = {} as paciente;
  @Output() recetaMedica = new EventEmitter<string>();
  /* @ViewChild('tomar') fileInput!: ElementRef;
  @ViewChild('frecuencia') fileInput1!: ElementRef;
  @ViewChild('duracion') fileInput2!: ElementRef; */
  public nombre = '';
  public edad = 0;
  public imc:any;
  public medico:medico = {} as medico;
  /* public activarMedicamento = false;
  public medicamentos:any[] = [];
  public medicamentosFiltrados:any[] = [];
  public medicamentosSeleccionados:any[] = [];
  public medicamentoSelected = ''; */

  constructor(public afs: AngularFirestore) { }

  ngOnInit(): void {
    setTimeout(() => {
      try {
        this.nombre = this.paciente.nombre.concat(' ',this.paciente.apellidoPaterno,' ',this.paciente.apellidoMaterno);
        this.edad = this.paciente.edad
      } catch (error) {
      }
    },1000)
    this.obtenerMedico();
  }

  /* obtenerMedicamentos(){
    this.medicamentos=[];
    this.afs.collection("Medicamentos").ref.where("uid","==", this.medico.uid).get()
    .then((query) =>{
      query.forEach((doc) =>{
        let id = {
          idMedicamento:doc.id
        }
        this.medicamentos.push(Object.assign(id,doc.data()));
      })
      this.medicamentos.sort((a,b)=> a.medicamento.codigo - b.medicamento.codigo);
    });
  } */

  formulario(form: NgForm){
    form.value.paciente = this.nombre
    form.value.edad = this.edad
    form.value.imc = this.imc
    this.recetaMedica.emit(form.form.value);
  }

  obtenerIMC(form: NgForm){
    this.imc = ((form.value.peso)/(form.value.talla * form.value.talla));
    let imcL = this.imc.toString();
    imcL.split(',', 2);
    let imcLn;
    imcLn = parseFloat(imcL).toFixed(2);
    this.imc = imcLn;
    form.value.imc = this.imc
  }

  /* buscarMedicamento(medicamento:any){
    if(medicamento.target.value.length != 0){
      if(medicamento.target.value.length > 3){
        this.activarMedicamento = true;
        this.medicamentosFiltrados = buscar(this.medicamentos,medicamento.target.value);
      }
    }else{
      this.activarMedicamento = false;
      this.medicamentosFiltrados = [];
    }
  } */

  /* seleccionarMedicamento(form:NgForm, item:any){
    this.medicamentoSelected = item.medicamento.nombreComercial
    this.medicamentosSeleccionados.push(item)
    this.activarMedicamento = false;
    this.medicamentosFiltrados = [];
  } */

  /* cantidad(event:any){
    Object.assign(this.medicamentosSeleccionados[this.medicamentosSeleccionados.length - 1],{tomar:event.target.value});
  }

  temporalidad(event:any){
    Object.assign(this.medicamentosSeleccionados[this.medicamentosSeleccionados.length - 1],{frecuencia:event.target.value});
  }

  dias(form:NgForm, event:any){
    Object.assign(this.medicamentosSeleccionados[this.medicamentosSeleccionados.length - 1],{duracion:event.target.value});
    this.medicamentoSelected = '';
    this.fileInput.nativeElement.value = ''
    this.fileInput1.nativeElement.value = ''
    this.fileInput2.nativeElement.value = ''
    form.value.paciente = this.nombre
    form.value.edad = this.edad
    form.value.imc = this.imc
    form.form.value.medicamento = this.medicamentosSeleccionados
    this.recetaMedica.emit(form.form.value);
  } */

  obtenerMedico(){
    this.medico = JSON.parse(localStorage.getItem('user')!);
    /* this.obtenerMedicamentos(); */
  }

}
