import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecendentesPersonalesPatologicosComponent } from './antecendentes-personales-patologicos.component';

describe('AntecendentesPersonalesPatologicosComponent', () => {
  let component: AntecendentesPersonalesPatologicosComponent;
  let fixture: ComponentFixture<AntecendentesPersonalesPatologicosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecendentesPersonalesPatologicosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecendentesPersonalesPatologicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
