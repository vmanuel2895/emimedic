import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { datosPP } from '../../../interfaces/historia-clinica';

@Component({
  selector: 'app-antecendentes-personales-patologicos',
  templateUrl: './antecendentes-personales-patologicos.component.html',
  styleUrls: ['./antecendentes-personales-patologicos.component.scss']
})
export class AntecendentesPersonalesPatologicosComponent implements OnInit {

  @Output() pato = new EventEmitter<string>();
  @Input() datos:datosPP = {
    hospitalizacionPrevia:'',
    atecedentesQuirurgicos:'',
    transfusionesPrevias:'',
    fracturas:'',
    otraEnfermedad:'',
    medicamentosActuales:'',
    acidenteTraumatismo:''
  } as datosPP;
  @Input() habilitar = false;

  constructor() { }

  ngOnInit(): void {
  }
  enviar(form:NgForm){
    this.pato.emit(form.form.value);
  }

}
