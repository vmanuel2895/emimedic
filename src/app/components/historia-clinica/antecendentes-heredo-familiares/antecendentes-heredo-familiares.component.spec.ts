import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecendentesHeredoFamiliaresComponent } from './antecendentes-heredo-familiares.component';

describe('AntecendentesHeredoFamiliaresComponent', () => {
  let component: AntecendentesHeredoFamiliaresComponent;
  let fixture: ComponentFixture<AntecendentesHeredoFamiliaresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecendentesHeredoFamiliaresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecendentesHeredoFamiliaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
