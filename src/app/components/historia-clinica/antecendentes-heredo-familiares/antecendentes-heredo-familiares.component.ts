import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-antecendentes-heredo-familiares',
  templateUrl: './antecendentes-heredo-familiares.component.html',
  styleUrls: ['./antecendentes-heredo-familiares.component.scss']
})
export class AntecendentesHeredoFamiliaresComponent implements OnInit {

  @Output() antecedentesHeredoFami = new EventEmitter<string>();
  @Input() datos = {
    diabetes:'',
    cancer:'',
    cardiopatias:'',
    malformaciones:'',
    otros:'',
    hipertencion:'',
    tipoHipertencion:'',
    nefropatas:'',
    tipoNefropatas:'',
  }

  @Input() habilitar = false;

  constructor() { }

  ngOnInit(): void {
  }

  enviar(form:NgForm){
    this.antecedentesHeredoFami.emit(form.form.value);
  }

}
