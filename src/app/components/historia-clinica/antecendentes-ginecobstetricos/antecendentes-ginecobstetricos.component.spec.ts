import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecendentesGinecobstetricosComponent } from './antecendentes-ginecobstetricos.component';

describe('AntecendentesGinecobstetricosComponent', () => {
  let component: AntecendentesGinecobstetricosComponent;
  let fixture: ComponentFixture<AntecendentesGinecobstetricosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecendentesGinecobstetricosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecendentesGinecobstetricosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
