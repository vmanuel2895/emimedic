import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { datosGineco } from '../../../interfaces/historia-clinica';

@Component({
  selector: 'app-antecendentes-ginecobstetricos',
  templateUrl: './antecendentes-ginecobstetricos.component.html',
  styleUrls: ['./antecendentes-ginecobstetricos.component.scss']
})
export class AntecendentesGinecobstetricosComponent implements OnInit {

  @Output() antecedentesGineco = new EventEmitter<string>();
  @Input() datos:datosGineco = {
    embarazada:'',
    embarazoRiesgo:'',
    metodoAnticonceptivo:''
  } as datosGineco;
  @Input() habilitar = false;

  constructor() { }

  ngOnInit(): void {
  }

  enviar(form:NgForm){
    this.antecedentesGineco.emit(form.form.value);
  }

}
