import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { datosPNP } from '../../../interfaces/historia-clinica';

@Component({
  selector: 'app-antecendentes-personales-no-patologicos',
  templateUrl: './antecendentes-personales-no-patologicos.component.html',
  styleUrls: ['./antecendentes-personales-no-patologicos.component.scss']
})
export class AntecendentesPersonalesNoPatologicosComponent implements OnInit {

  @Output() antecedentesPersonalesNoPatologicos = new EventEmitter<string>();
  @Input() datos:datosPNP = {
    vivienda:'',
    tipoSanguineo:'',
    alergias:"",
    alimentacion:''
  } as datosPNP;
  @Input() habilitar = false;

  constructor() { }

  ngOnInit(): void {
  }

  enviar(form:NgForm){
    this.antecedentesPersonalesNoPatologicos.emit(form.form.value);
  }

}
