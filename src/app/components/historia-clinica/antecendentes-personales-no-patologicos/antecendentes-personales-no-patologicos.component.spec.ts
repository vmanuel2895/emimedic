import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AntecendentesPersonalesNoPatologicosComponent } from './antecendentes-personales-no-patologicos.component';

describe('AntecendentesPersonalesNoPatologicosComponent', () => {
  let component: AntecendentesPersonalesNoPatologicosComponent;
  let fixture: ComponentFixture<AntecendentesPersonalesNoPatologicosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AntecendentesPersonalesNoPatologicosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AntecendentesPersonalesNoPatologicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
