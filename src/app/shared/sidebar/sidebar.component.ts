import { Component, OnInit } from '@angular/core';
import { SidebarService } from 'src/app/services/sidebar/sidebar.service';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[] = [];
  public image='../../../assets/logoMedic/modo oscuro/doctores.svg'

  constructor( private sidebarService: SidebarService,
              private _login:LoginService ) {
    sidebarService.menu.forEach((element:any)=>{
      if(element.role == undefined){
        this.menuItems.push(element);
      }else if(element.role == JSON.parse(localStorage.getItem('user')!).role){
        this.menuItems.push(element);
      }
    })
    /* this.menuItems = sidebarService.menu; */
  }

  ngOnInit(): void {
  }

  imagen(event:any){
    if (event.titulo == 'INICIO') {
      this.image = '../../../assets/logoMedic/modo oscuro/doctores.svg'
    } else if(event.titulo == 'PACIENTES'){
      this.image = '../../../assets/logoMedic/modo oscuro/registro.svg'
    }else if(event.titulo == 'COSULTA MÉDICA'){
      this.image = '../../../assets/logoMedic/modo oscuro/receta.svg'
    }else if(event.titulo == 'BITÁCORA'){
      this.image = '../../../assets/logoMedic/modo oscuro/registro.svg'
    }else if(event.titulo == 'FARMACIA'){
      this.image = '../../../assets/logoMedic/modo oscuro/registro.svg'
    }
  }

  cerrarSesion(){
    this._login.logout();
  }

}
