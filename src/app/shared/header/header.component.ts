import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { User,LoginService } from '../../services/login/login.service';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('filterName') fileInput!: ElementRef;
  public doctor={
    displayName:"",
    email:'',
    photoURL:'',
    uid:'',
    genero:''
  }
  public dataPacientes : any [] = []
  public pacientesEncontrados:any [] = [];

  constructor(private _login:LoginService,
              public afs: AngularFirestore) { }

  ngOnInit(): void {
    this.obtenerDoctor();
    this.obtenerDataPacientes();
  }

  //funcion para cargar la foto de perfil del usuario
  async upload() {
    const { value: file } = await Swal.fire({
      title: 'Selecciona una image',
      input: 'file',
      inputAttributes: {
        'accept': 'image/*',
        'aria-label': 'Upload your profile picture'
      }
    })

    if (file) {
      const reader = new FileReader()
      reader.onload = (e) => {
      }
      reader.readAsDataURL(file)
      this._login.subirImagen(file)
      setTimeout(() => {
        this.obtenerDoctor();
      },5000)
    }
  }
  //fin de la funcion

  async uploadEscuela(){
    const { value: fil } = await Swal.fire({
      title: 'Ingresa el logo de tu institucion',
      input: 'file',
      inputAttributes: {
        'accept': 'image/*',
        'aria-label': 'Upload your profile picture'
      }
    })
    if (fil) {
      const reader = new FileReader()
      reader.onload = (e) => {
      }
      reader.readAsDataURL(fil)
      this._login.subirImagenInstitucion(fil,this.doctor)
      setTimeout(() => {
        this.obtenerDoctor();
      },5000)
    }
  }

  //Funcion para obtener al doctor
  obtenerDoctor(){
    this.doctor ={
      displayName:"",
      email:'',
      photoURL:'',
      uid:'',
      genero:''
    }
    let medico = JSON.parse(localStorage.getItem('user')!)
    this.doctor.photoURL = medico.photoURL;
    this.doctor.displayName = medico.displayName;
    this.doctor.uid = medico.uid;
    this.doctor.genero = medico.genero;
  }
  //Fin de la funcion

  // funcion para obtener pacientes
  obtenerDataPacientes(){
    this.afs.collection("Pacientes").ref.where("uid","==", this.doctor.uid).get()
    .then((query) =>{
      this.dataPacientes=[]
      query.forEach((doc) =>{
        let id = {
          idPaciente:doc.id
        }
        const returnedTarget = Object.assign(id, doc.data());
        this.dataPacientes.push(returnedTarget);
      })
    })
  }
  //fin obtener data pacientes

  //funcion para buscar los pacientes del doctor
  buscarPaciente(event:any){
    this.obtenerDataPacientes();
    this.pacientesEncontrados = [];
    if (event.target.value == '') {
      this.pacientesEncontrados = [];
    } else if (event.target.value.length >= 3){
      this.dataPacientes.forEach((paciente) => {
        let nombreCompleto = paciente.nombre.concat(' ',paciente.apellidoPaterno,' ',paciente.apellidoMaterno);
        if (nombreCompleto.toLowerCase().includes(event.target.value.toLowerCase())) {
          this.pacientesEncontrados.push(paciente)
        }
      })
    }
  }
  //fin de la funcion

  //funcion para agregar al paciente al local storage
  async agregarPaciente(paciente:any){
    this.pacientesEncontrados = [];
    this.fileInput.nativeElement.value = '';
    await localStorage.setItem('paciente', JSON.stringify(paciente))
  }
  //fin de la funcion

  //funcion para cerrar sesion
  cerrarSesion(){
    this._login.logout();
  }
  //fin de la funcion
}
