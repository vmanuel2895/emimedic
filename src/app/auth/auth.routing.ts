import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

/* import { RegisterComponent } from './register/register.component'; */
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { TerminosCondicionesComponent } from './terminos/terminos-condiciones/terminos-condiciones.component';

const routes: Routes = [

    /* { path: 'register', component: RegisterComponent }, */
    { path: 'login', component: LoginComponent },
    { path: 'registro', component: RegistroComponent },
    { path: 'terminos/condiciones', component: TerminosCondicionesComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule {}
