import { expressionType } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { regExpEscape } from '@ng-bootstrap/ng-bootstrap/util/util';
import { invalid } from 'moment';
import { LoginService } from 'src/app/services/login/login.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})

export class RegistroComponent implements OnInit {
  public save = true;
  public pass  = NgForm.length;

  constructor(public _Resgitro:LoginService) { }

  ngOnInit(): void {
  }

  //Funcion de registro de Doctor
  async Registro(form:NgForm){
    if(form.status == "INVALID"){
      Swal.fire({
        position: 'top-end',
        icon: 'warning',
        title: 'DATOS INCOMPLETOS',
        text: 'POR FAVOR LLENAR TODOS LOS COMPOS',
        showConfirmButton: false,
        timer: 1500
      })
    }else {
      const { email, pass } = form.value;
      let data = Object.assign(form.value,{genero:this.obtenerGenero(form.value.curp)})
      this._Resgitro.registrarUser(email,pass,data);
    }
  }

  obtenerGenero(curp:string){
    let genero='';
    let gen = curp.slice(10,-7);
    gen.toUpperCase();
    if(gen != null || gen != undefined){
      if(gen == 'H'){
        genero = 'MASCULINO'
      }else if (gen == 'M'){
        genero = 'FEMENINO'
      }
    }
    return genero;
  }

  // Validacion de password
  validarPass(form:NgForm){
    const {pass} = form.value
    if(pass.length <8 || pass.length == 0 || pass ==="" || pass === null){
      Swal.fire({
        position: 'top-end',
        icon: 'warning',
        title: 'Campo incompleto',
        text: 'Debe tener minimo 8 caracteres',
        showConfirmButton: false,
        timer: 1500
      })
    }
  }// fin validar pass

  // validar curp
  validarCurp(form:NgForm){
    if(form.controls.curp.status == "INVALID" || form.controls.curp.value == ""){
      Swal.fire({
        position: 'top-end',
        icon: 'warning',
        title: 'CURP INVALIDO',
        text: 'Ingrese un CURP valido',
        showConfirmButton: false,
        timer: 1500
      })
      form.controls.curp.reset();
    }
  }//fin calidarCurp
}
