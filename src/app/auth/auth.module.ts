import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { TerminosCondicionesComponent } from './terminos/terminos-condiciones/terminos-condiciones.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegistroComponent,
    TerminosCondicionesComponent,
  ],
  exports: [
    LoginComponent,
    RegistroComponent,
    TerminosCondicionesComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ]
})
export class AuthModule { }
