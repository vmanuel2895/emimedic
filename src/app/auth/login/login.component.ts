import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { LoginService } from 'src/app/services/login/login.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public _login:LoginService) {
    this._login.logout();
  }

  ngOnInit(): void {

  }

  async login(form: NgForm){
    if (form.status != 'INVALID') {
      const { usuario, password } = form.value;
      this._login.login(usuario, password);
    } else {
      form.resetForm();
    }
  }

  async cambiarPass(){
    const { value: email } = await Swal.fire({
      title: 'Recuperar Contraseña',
      input: 'email',
      inputLabel: 'Correo Electronico',
      inputPlaceholder: 'Ingresa tu correo electronico',
      confirmButtonText: 'Enviar',
    })
    if (email) {
      this._login.recuperarPassword(email);
    }
  }

  instagram(){
    window.open("https://www.instagram.com/emiexsecof/", "_blank");
  }

  facebook(){
    window.open("https://www.facebook.com/emiexsec", "_blank");
  }

  whatsapp(){
    window.open("https://wa.me/527778313325/?text=Hola%20EMIEXSEC,", "_blank");
  }

  soporte(){
    Swal.fire({
      title: 'SOPORTE TECNICO',
      html:
        'HORARIO DE ATENCION:'+
        '<li style="text-align: center; list-style:none;">LUNES A VIERNES DE 9:00AM A 5:00PM</li> <li style="text-align: center; list-style:none;">SABADO Y DOMINGO DE 10:00AM A 3:00PM</li>'+
        '<br>CONTACTO:'+
        '<p style="text-align: center;">735-418-20-83</p>',
      icon: 'info',
      showCancelButton: true,
      cancelButtonText: `OK`,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#B2C9F7',
      confirmButtonText: 'MANUAL DE USUARIO'
    }).then((result) => {
      if (result.isConfirmed) {
        window.open('https://drive.google.com/file/d/1nNSDNeT2uucLFIAdxFxlCYKm2PhGUCkx/view?usp=sharing');
      }
    })
    /* Swal.fire({
      icon: 'info',
      title: 'SOPORTE TECNICO',
      html:
        'HORARIO DE ATENCION:'+
        '<li style="text-align: left;">LUNES A VIERNES DE 9:00AM A 5:00PM</li> <li style="text-align: left;">SABADO Y DOMINGO DE 10:00AM A 3:00PM</li>'+
        '<br>CONTACTO:'+
        '<p style="text-align: center;">735-418-20-83</p>'
    }) */
    /* window.open('https://drive.google.com/file/d/1nNSDNeT2uucLFIAdxFxlCYKm2PhGUCkx/view?usp=sharing'); */
  }

}
