import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriaClinicaCompletaComponent } from './historia-clinica-completa.component';

describe('HistoriaClinicaCompletaComponent', () => {
  let component: HistoriaClinicaCompletaComponent;
  let fixture: ComponentFixture<HistoriaClinicaCompletaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoriaClinicaCompletaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriaClinicaCompletaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
