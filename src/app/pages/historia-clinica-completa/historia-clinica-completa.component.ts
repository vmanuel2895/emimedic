import { Component, OnInit } from '@angular/core';
import { medico } from 'src/app/interfaces/medico';
import { search } from 'src/app/interfaces/paciente';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-historia-clinica-completa',
  templateUrl: './historia-clinica-completa.component.html',
  styleUrls: ['./historia-clinica-completa.component.scss']
})
export class HistoriaClinicaCompletaComponent implements OnInit {

  public antecedentesHeredoFamiliares= {};
  public antecedentesPersonalesPatologicos = {};
  public antecedentesPersonalesNoPatologicos = {};
  public antecedentesGinecologicos = {};
  public paciente:search = {} as search;
  public user:medico = {} as medico;
  public genero = '';
  public edad = 0;

  constructor(private _paciente:PacientesService) { }

  ngOnInit(): void {
    this.paciente = JSON.parse(localStorage.getItem('paciente')!);
    this.user = JSON.parse(localStorage.getItem('user')!);
    this.genero = this.paciente.sexo;
    this.edad = parseInt(this.paciente.edad)
  }

  familiares(event:any){
    this.antecedentesHeredoFamiliares = event
  }

  patologico(event:any){
    this.antecedentesPersonalesPatologicos = event
  }

  noPatologicos(event:any){
    this.antecedentesPersonalesNoPatologicos = event
  }

  gineco(event:any){
    this.antecedentesGinecologicos = event
  }

  objetoVacio(obj:any,genero:any) {
    let contador = 0
    let validar = false
    for (var prop in obj) {
      if(obj[prop] !== undefined){
        contador+=1
      }
    }
    if(genero == 'Masculino'){
      if(contador == 38){
        validar = true
      }else{
        validar = false
      }
    }else{
      if(this.edad > 12){
        if(contador == 61){
          validar = true
        }else{
          validar = false
        }
      }else{
        if(contador == 38){
          validar = true
        }else{
          validar = false
        }
      }
    }
    return validar;
  }

  guardarHitoriaClinica(){
    let data = Object.assign(this.antecedentesHeredoFamiliares,
      this.antecedentesPersonalesPatologicos,
      this.antecedentesPersonalesNoPatologicos,
      this.antecedentesGinecologicos);
    let valid = this.objetoVacio(data, this.paciente.sexo)
    if(valid){
      this._paciente.setHistoriaClinicaCompleta(this.user, data, this.paciente);
    }else{
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'ERROR DE LLENADO',
        text: 'ES NECESARIO LLENAR COMPLETAMENTE LA HISTORIA CLÍNICA',
        showConfirmButton: false,
        timer: 1500
      })
    }
  }

}
