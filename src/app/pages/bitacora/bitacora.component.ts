import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { PacientesService } from '../../services/pacientes/pacientes.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { medico } from '../../interfaces/medico'
import { DatePipe } from '@angular/common';
import { element } from 'protractor';
import { DateAdapter } from 'angular-calendar';
import { startOfDay } from 'date-fns';
import { clear, timeStamp } from 'console';
import Swal from 'sweetalert2'
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { comparar } from "../../functions/comparacionFechas";

@Component({
  selector: 'app-bitacora',
  templateUrl: './bitacora.component.html',
  styleUrls: ['./bitacora.component.scss']
})
export class BitacoraComponent implements OnInit {

  @ViewChild ("FilterName") 'FilterName' : ElementRef;
  public user:medico = {} as medico;
  /* public user ={
    apellidoMaterno: "",
    apellidoPaterno: "",
    cedula: "",
    curp: "",
    displayName: "",
    email: "",
    emailVerified: "",
    nombre: "",
    photoURL: "",
    telefono: "",
    uid: ""
  } */
  public consultas:any=[];
  public datos = {};


  public porFecha :any =[];
  public valorDesde:any;
  // public verFecha = new Date(this.valorDesde);
  public valorHasta:any;

  constructor(public afs: AngularFirestore) { }

  ngOnInit(): void {
    this.obtenerDoctor();
    this.obtenerHistoricoConsultas();

    // this.obtenerFecha();
  }

  //funcion para obtener al doctor del localstorage
  obtenerDoctor(){
    this.user = JSON.parse(localStorage.getItem('user')!)
  }

  //funcion para obtener el historial de consultas
  obtenerHistoricoConsultas(){
    this.afs.collection("HistoriaClinica").ref.where("uid","==", this.user.uid).get()
    .then((query) =>{
      query.forEach((doc) =>{
          this.datos = doc.data();
          this.porFecha.push(this.datos)
      })
    })
    /* this.porFecha=[] */
  }
  //fin de la funcion

  //funcion para obtener solo por fecha
  obtenerFecha(){
    this.porFecha = [];
    this.afs.collection("HistoriaClinica").ref
    .where("uid","==", this.user.uid)
    .get()
    .then((query) =>{
      query.forEach((doc) =>{
        let fecha = new Date(doc.data().fechaInicio).toISOString().split('T')[0]
        let valor = comparar(fecha, this.valorDesde, this.valorHasta);
        if(valor){
          this.porFecha.push(doc.data())
        }
      })
    })
  }// fin fecha



  DescargarPDF(){
    const doc : any = new jsPDF({ orientation: 'landscape'});
    const data : any =[]
    var contador =1
    this.porFecha.forEach((element:any) => {
      var today = new Date(element.fechaInicio);

      var x =[
        contador,
        today.toLocaleDateString("es-MX"),
        element.paciente.nombre+" " + element.paciente.apellidoPaterno+" " +element.paciente.apellidoMaterno,
        element.paciente.edad
    ]
    contador = contador + 1;
      data.push(x)
    });
    doc.setTextColor(100);
    doc.setFont('courier')
    doc.text('BITÁCORA DE CONSULTAS', 110, 20);
    doc.autoTable({
      styles: { halign: 'center',font :'courier',fontSize:18 },
      columnStyles: {  halign: 'center',font :'courier',fontSize:12  },
      head:[["No.","FECHA","NOMBRE DEL PACIENTE","EDAD"]],
      body :data,
      startY:30
    });

    doc.text("__________________________",10,170)
    doc.text("FIRMA",35,175)
    doc.text("Dr. "+this.user.nombre +" " +this.user.apellidoPaterno +" "+ this.user.apellidoMaterno,10,180)
    doc.text("Cedúla: " + this.user.cedula,10,190)

    doc.text("CONSULTORIO: ",160,175)
    doc.text("CALLE: "+this.user.consultorio +" "+ this.user.numeroExterior + " " + this.user.colonia,160,180)
    doc.text(this.user.municipio +", " + this.user.estado ,160,185)
    doc.text("C.P.: " + this.user.codigoPostal,160,190)
    doc.text("TELEFONO: "+" "+this.user.telefono,160,195)
    // Open PDF document in new tab
      doc.output('dataurlnewwindow')

    // doc.save("Bitácora");
  }
}
