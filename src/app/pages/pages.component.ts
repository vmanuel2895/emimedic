import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

declare function customInitFunctions():void;

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    customInitFunctions();
  }

  soporte(){
    Swal.fire({
      icon: 'info',
      title: 'SOPORTE TECNICO',
      html:
        'HORARIO DE ATENCION:'+
        '<p style="text-align: center;">LUNES A VIERNES DE 9:00AM A 5:00PM</p> <p style="text-align: center;">SABADO Y DOMINGO DE 10:00AM A 3:00PM</p>'+
        '<br>CONTACTO:'+
        '<p style="text-align: center;">735-418-20-83</p>'+
        '<a href="https://www.teamviewer.com/es-mx/descarga/windows/" target="_blank">DESCARGA TEAMVIEWER</a> '
    })
    /* Swal.fire({
      title: '<strong>SOPORTE TECNICO</strong>',
      icon: 'info',
      html:
        'You can use <b>bold text</b>, ' +
        '<a href="//sweetalert2.github.io">links</a> ' +
        'and other HTML tags',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i>',
      cancelButtonAriaLabel: 'Thumbs down'
    }) */
  }

  cuenta(){
    Swal.fire({
      icon: 'info',
      title: 'NUMERO DE CUENTA SANTANDER',
      html:
        '<br>'+
        '<p style="text-align: center;">NOMBRE: </p> <p style="text-align: center;">DAVID ORTIZ VALENCIA</p>'+
        '<p style="text-align: center;">NUMERO DE CUENTA: </p> <p style="text-align: center;">56778924259</p>'+
        '<p style="text-align: center;">NUMERO DE TARJETA: </p> <p style="text-align: center;">5579100361546474</p>'+
        '<p style="text-align: center;">CUENTA CLABE: </p> <p style="text-align: center;">014542567789242590</p>'
    })
  }

}
