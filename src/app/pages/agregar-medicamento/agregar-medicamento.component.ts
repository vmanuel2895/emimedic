import { Component, Input, OnInit } from '@angular/core';
import { FarmaciaService } from '../../services/farmacia/farmacia.service'
import { medicamento } from '../../interfaces/medicamentos';
import Swal from 'sweetalert2';
import { AngularFirestore} from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { medico } from 'src/app/interfaces/medico';

@Component({
  selector: 'app-agregar-medicamento',
  templateUrl: './agregar-medicamento.component.html',
  styleUrls: ['./agregar-medicamento.component.scss']
})
export class AgregarMedicamentoComponent implements OnInit {

  @Input() datos:medicamento = {
    formaFarmaceutica:'',
  } as medicamento;
  public fecha = new Date();
  public user:medico = {} as medico;
  public guardar = true;
  public editar = false;

  constructor(public medicamentos:FarmaciaService,
              public afs: AngularFirestore,
              private router:Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('medicamento') != null || localStorage.getItem('medicamento') != undefined){
      this.datos = JSON.parse(localStorage.getItem('medicamento')!);
      this.editar = true;
      this.guardar = false
    }
    this.user = JSON.parse(localStorage.getItem('user')!)
    this.obtenerMedicamentos()
  }

  obtenerMedicamentos(){
    let conteo:any[]=[];
    this.afs.collection("Medicamentos").ref.where("uid","==", this.user.uid).get()
    .then((query) =>{
      query.forEach((doc) =>{
        conteo.push(doc.data());
      })
      localStorage.setItem('medicamentos',(conteo.length+1).toString());
    });
  }

  sumar(cantidad:any){
    this.datos.cantidad += parseInt(cantidad.target.value);
  }

  agregar(){
    try {
      let count = localStorage.getItem('medicamentos')
      this.medicamentos.agregarMedicamentos(this.user,Object.assign(this.datos,{fechaCreacion:this.fecha},{codigo:parseInt(count!)}));
      this.datos = {
        formaFarmaceutica:'',
      } as medicamento;
      this.obtenerMedicamentos();
      this.router.navigate(['dashboard/medicamentos']);
    } catch (error) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'ERROR',
        text: 'ERROR AL GUARDAR MEDICAMENTO',
        showConfirmButton: false,
        timer: 1500
      })
    }
  }

  editarMedicamento(){
    let id = JSON.parse(localStorage.getItem('medicamento')!).idMedicamento
    this.afs.collection("Medicamentos").doc(id)
    .update({ medicamento: this.datos }).then(() => {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'EL MEDICAMENTO SE ACTUALIZO CORRECTAMENTE',
        text: 'SE ACTUALIZO EL MEDICAMENTO',
        showConfirmButton: false,
        timer: 1500
      });
      localStorage.removeItem('medicamento')
      this.router.navigate(['dashboard/medicamentos']);
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
  }

}
