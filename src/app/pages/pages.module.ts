import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../components/components.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { PacientesComponent } from './pacientes/pacientes.component';
import { HistoriaClinicaComponent } from './historia-clinica/historia-clinica.component';
import { BitacoraComponent } from './bitacora/bitacora.component';

import { FlatpickrModule } from 'angularx-flatpickr';
import 'flatpickr/dist/flatpickr.css';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { HistoriaClinicaCompletaComponent } from './historia-clinica-completa/historia-clinica-completa.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { PerfilComponent } from './perfil/perfil.component';
import { FarmaciaComponent } from './farmacia/farmacia.component';
import { AgregarMedicamentoComponent } from './agregar-medicamento/agregar-medicamento.component';
import { DatepickerModule } from 'ng2-datepicker';
import { DatosFacturacionComponent } from './datos-facturacion/datos-facturacion.component';
import { ActivarDoctoresComponent } from './activar-doctores/activar-doctores.component';

@NgModule({
  declarations: [
    DashboardComponent,
    PagesComponent,
    PacientesComponent,
    HistoriaClinicaComponent,
    BitacoraComponent,
    HistoriaClinicaCompletaComponent,
    PerfilComponent,
    FarmaciaComponent,
    AgregarMedicamentoComponent,
    DatosFacturacionComponent,
    ActivarDoctoresComponent,
  ],
  exports: [
    DashboardComponent,
    PagesComponent,
    PacientesComponent,
    HistoriaClinicaComponent,
    BitacoraComponent,
    FarmaciaComponent,
    AgregarMedicamentoComponent,
    DatosFacturacionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule,
    ComponentsModule,
    FormsModule,
    FlatpickrModule.forRoot(),
    NgbModalModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    NgxSpinnerModule,
    DatepickerModule
  ]
})
export class PagesModule { }
