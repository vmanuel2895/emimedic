import { Component, OnInit } from '@angular/core';
import { medico } from 'src/app/interfaces/medico';
import { AngularFirestore } from '@angular/fire/firestore';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {

  public user:medico = {} as medico;

  constructor(public afs: AngularFirestore, public _login:LoginService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user')!)
  }

  actualizar(form:NgForm){
    let datos = Object.assign(form.form.value,{displayName:form.form.value.nombre})
    this.afs.collection("Doctores").doc(this.user.uid)
    .update(datos).then(()=>{
      this._login.obtenerDoctor();
      this._login.actualizarPerfilNombre(form.form.value);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'PERFIL ACTUALIZADO CORRECTAMENTE',
        text: 'PERFIL ACTUALIZADO CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
    })
  }

}
