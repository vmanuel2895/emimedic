import { Component, OnInit, Output } from '@angular/core';
import { paciente } from '../../interfaces/paciente';
import Swal from 'sweetalert2'
import { PacientesService } from '../../services/pacientes/pacientes.service';
import { medico } from '../../interfaces/medico';
import { Cie10Service } from '../../services/cie10/cie10.service';
import jsPDF from 'jspdf';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-historia-clinica',
  templateUrl: './historia-clinica.component.html',
  styleUrls: ['./historia-clinica.component.scss']
})
export class HistoriaClinicaComponent implements OnInit {

  public user:medico = {} as medico;
  public paciente:paciente = {} as paciente;
  public recetaMedica = {}
  public estudiosPaciente = {}
  public recetas =""
  public signos ={ edad :"", talla:"", peso:"",imc:"",ta:"",temp:"",alergias:""}
  public notaMedic = {
    cie10:[]
  }

  constructor(private _paciente:PacientesService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user')!)
    this.obtenerPaciente();
  }

  receta(event:any){
    this.recetaMedica = event;
    this.recetas = event.nota
    this.signos.edad=event.edad
    this.signos.peso=event.peso
    this.signos.talla=event.talla
    this.signos.imc=event.imc
    this.signos.ta=event.ta
    this.signos.temp=event.temp
    this.signos.alergias=event.alergias
  }

  estudios(event:any){
    this.estudiosPaciente = event;
  }

  notaMedico(event:any){
    this.notaMedic.cie10 = event;
  }

  obtenerPaciente(){
    this.paciente = JSON.parse(localStorage.getItem('paciente')!)
  }

  enviar(){
    this.spinner.show();
    setTimeout(() => {
      let historia = Object.assign(this.notaMedic,
                                   this.estudiosPaciente,
                                   this.recetaMedica);
        this._paciente.setHistoriaClinica(this.user, historia, this.paciente);
      this.recetaPDF();
    }, 3000);
  }

  recetaPDF(){
    let photoInst = JSON.parse(localStorage.getItem('user')!).photoIntitucion
    var fechas =  Date.now()
    const hoy = new Date(fechas)
    const doc : any = new jsPDF()
    doc.setTextColor(100);
    doc.setFont('arial')
    doc.setFontSize(10);

    var img = new Image()


    img.src = 'assets/images/encabezado.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
    doc.addImage(img, 'png', 10, 10, 20, 40)

    var img2 = new Image()
    img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
    doc.addImage(img2, 'png', 5, 50, 210, 3)

    var img3 = new Image()
    img3.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
    doc.addImage(img3, 'png', 5, 250, 210, 3)

    var img4 = new Image()
    img4.src = 'assets/images/fondomedic.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
    doc.addImage(img4, 'png', 70, 80, 70, 100)

    var fotoInstitu = new Image();
    fotoInstitu.src = photoInst
    doc.addImage(fotoInstitu, 'JPEG' , 165, 5, 40, 40)


    doc.text("DR." + this.user.nombre +" "+ this.user.apellidoPaterno +" " + this.user.apellidoMaterno,60,20)
    doc.text(this.user.universidad,60,25)
    doc.text("CÉDULA PROFESIONAL "+this.user.cedula,60,30)
    doc.text(this.user.titulo,60,35)
    doc.text("Fecha: " + hoy.toLocaleDateString(undefined,{year:'numeric',month:'long',day:'numeric'}),10,70)
    doc.text(" Hora: "+hoy.toLocaleTimeString('en-US'),170,70)



    doc.text("Edad: "+this.signos.edad +" años",170,90)
    doc.text("Peso: "+this.signos.peso +" Kg",170,100)
    doc.text("Talla: "+this.signos.talla+" Mts",170,110)
    doc.text("Ta: "+this.signos.ta +" mmHg",170,120)
    doc.text("IMC: "+this.signos.imc +" Kg/m²",170,130)
    doc.text("Temp: "+this.signos.temp +" °C",170,140)
    doc.text("Alergias: " + this.signos.alergias,170,150)
    doc.text("Paciente (a): " + this.paciente.nombre +" "+ this.paciente.apellidoPaterno+" "+this.paciente.apellidoMaterno,10,90)
    doc.text("Dx: "+ this.recetas.toUpperCase(),10,100)

    // doc.text("Nombre y firma del medico tratante",100,125)
    // doc.text(this.user.nombre +" "+ this.user.apellidoPaterno +" " +this.user.apellidoMaterno,100,130)
     doc.text("______________________________________",130,240)
     doc.text("FIRMA",160,245)

     doc.text(this.user.consultorio +" "+ this.user.numeroExterior +" "+ this.user.colonia +" C.P: "+this.user.codigoPostal +", " +this.user.municipio+", "+this.user.estado,450,270)
     doc.text("PREVIA CITA TELEFONICA",80,275)
     doc.text("TEL: " +this.user.telefono,80,280)
    // doc.text("Cédula: "+ this.user.cedula,100,140)

    // doc.output('dataurlnewwindow')
    this.spinner.hide();
    doc.save("RECETA MEDICA")
  }

}
