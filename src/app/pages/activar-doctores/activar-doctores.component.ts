import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoginService } from 'src/app/services/login/login.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-activar-doctores',
  templateUrl: './activar-doctores.component.html',
  styleUrls: ['./activar-doctores.component.scss']
})
export class ActivarDoctoresComponent implements OnInit {

  public doctores:any [] = []

  constructor(public afs: AngularFirestore, public _login:LoginService) { }

  ngOnInit(): void {
    this.obtenerDoctores();
  }

  obtenerDoctores(){
    this.afs.collection("Doctores").ref.get()
    .then((query) =>{
      query.forEach((doc) =>{
        this.doctores.push(doc.data());
        console.log(doc.data());
      })
    });
  }

  desactivarUsuario(doctor:any){
    doctor.stado = false
    this.afs.collection("Doctores").doc(doctor.uid)
    .update(doctor).then(()=>{
      this._login.obtenerDoctor();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'SE DESACTIVO CORRECTAMENTE EL DOCTOR',
        text: 'DOCTOR DESACTICADO CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
    })
  }

  activarUsuario(doctor:any){
    doctor.stado = true
    this.afs.collection("Doctores").doc(doctor.uid)
    .update(doctor).then(()=>{
      this._login.obtenerDoctor();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'SE ACTIVO CORRECTAMENTE EL DOCTOR',
        text: 'DOCTOR ACTIVO CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
    })
  }

}
