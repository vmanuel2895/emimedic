import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivarDoctoresComponent } from './activar-doctores.component';

describe('ActivarDoctoresComponent', () => {
  let component: ActivarDoctoresComponent;
  let fixture: ComponentFixture<ActivarDoctoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivarDoctoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivarDoctoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
