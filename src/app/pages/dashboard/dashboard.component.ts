import { Component, ChangeDetectionStrategy, ViewChild, TemplateRef, OnInit, ElementRef, } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay,isSameMonth,addHours,} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent,CalendarView,} from 'angular-calendar';
import { User, LoginService } from '../../services/login/login.service'
import { NgForm } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { paciente } from '../../interfaces/paciente';
import { AgendaService } from '../../services/agenda/agenda.service';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};
@Component({
  selector: 'app-dashboard',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit{
  @ViewChild('modalContent', { static: true }) modalContent?: TemplateRef<any>;
  @ViewChild('modalContent2', { static: true }) modalContent2?: TemplateRef<any>;
  public dataPacientes : any [] = []
  public pacientesEncontrados:any [] = [];
  public citasEncontradas:any [] = [];
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  modalData?: {
    action: string;
    event: CalendarEvent;
  };
  refresh: Subject<any> = new Subject();
  events: CalendarEvent[] = [];
  public eventos2:any[] = [];
  activeDayIsOpen: boolean = true;
  public doctor:User = {} as User;
  public dia='';
  public paciente:paciente = {} as paciente;
  public hoy = new Date().toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' });
  public day = startOfDay(new Date());
  public agenda:any = {
    color: "",
    draggable: false,
    end:"",
    motivo: "",
    resizable: "",
    start: "",
    title: "",
    uid: "",
  }
  public nombreEncontrado = {
    nombre:''
  }

  constructor(private modal: NgbModal,
              public afs: AngularFirestore,
              private _agenda:AgendaService) {
              let data = JSON.parse(localStorage.getItem('agenda')!)
              if(data != null){
                data.forEach((elemen:any) => {
                  let evento = {
                    title: elemen.title,
                    start: new Date(elemen.start),
                    end: new Date(elemen.end),
                    color: elemen.color,
                    draggable: elemen.draggable,
                    resizable: {
                      beforeStart: elemen.resizable.beforeStart,
                      afterEnd: elemen.resizable.afterEnd,
                    },
                    motivo:elemen.motivo
                  }
                  this.agregarEvento(evento);
                });
              }
            }

  ngOnInit(): void {
    this.obtenerUsuario();
    this.eliminarLocal();
    this.obtenerAgenda(this.day);
    this.dayClicked({
      date: this.day,
      events: this.events
    });
  }

  agregarEventos(event:any){
    this.events = [
      ...this.events,
      event,
    ];
  }

  obtenerAgenda(dia:Date){
    this.afs.collection("Agenda").ref.where("uid","==", this.doctor.uid)
    .onSnapshot((querySnapshot) => {
      this.events=[]
      this.eventos2=[];
      querySnapshot.forEach((doc) => {
        doc.data().evento.forEach((elemen:any) => {
          let evento = {
            title: elemen.title,
            start: new Date(elemen.start.toDate()),
            end: new Date(elemen.end.toDate()),
            color: elemen.color,
            draggable: elemen.draggable,
            resizable: {
              beforeStart: elemen.resizable.beforeStart,
              afterEnd: elemen.resizable.afterEnd,
            },
            uid:doc.id,
            motivo:elemen.motivo
          }
          this.agregarEvento(evento);
            if(evento.start.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' }) == dia.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' })){
              this.eventos2 = [
                ...this.eventos2,
                evento
              ];
            }

          this.setView(this.CalendarView.Month);
        });
        localStorage.setItem('agenda',JSON.stringify(this.events))
        localStorage.setItem('agenda2',JSON.stringify(this.eventos2))
      });
    });
  }

  //Funcion para obtener usuario logueado
  obtenerUsuario(){
    this.doctor = JSON.parse(localStorage.getItem('user')!);
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    this.dia = date.toDateString();
    this.eventos2=[];
    this.events.forEach((element:any)=>{
      if(element.start.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' }) == date.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' })){
        this.eventos2.push(element)
      }
    })
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ){
        this.activeDayIsOpen = false;
      }else{
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({ event,newStart,newEnd,}: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    event.start = newStart
    event.end = newEnd
    let valor = this.eventos2.indexOf(event)
    let valorEn= this.eventos2[valor]
    this._agenda.actualizarCita(valorEn);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.detalleConsulta(event);
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  agendar(){
    this.modal.open(this.modalContent2, { size: 'lg' });
  }

  detalleConsulta(event:any){
    for (const key in this.agenda) {
      if (event[key] == undefined) {
        this.agenda[key] = ''
      }else{
        this.agenda[key] = event[key]
      }
    }
  }

  agregarEvento(data:any){
    this.events = [
      ...this.events,
      data
    ];
  }

  addEvent(form:NgForm): void {
    var array: CalendarEvent[] = [];
    var currentDateObj = new Date(form.value.horario);
    var numberOfMlSeconds = currentDateObj.getTime();
    var addMlSeconds = 30 * 60000;
    var newDateObj = new Date(numberOfMlSeconds + addMlSeconds);
    var evento = {
      title: this.paciente.nombre.concat(' ', this.paciente.apellidoPaterno, ' ',this.paciente.apellidoMaterno),
      start: new Date(form.value.horario),
      end: newDateObj,
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      motivo:form.value.motivo
    }
    array.push(evento);
    this._agenda.agendar(this.doctor, array, this.paciente);
    this.events = [
      ...this.events,
      evento
    ];
    if(evento.start.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' }) == this.day.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' })){
      this.eventos2 = [
        ...this.eventos2,
        evento
      ];
    }
    this.eventos2.sort(function (a,b){
      if (a.start.getTime() > b.start.getTime()) {
        return 1;
      }
      if (a.start.getTime() < b.start.getTime()) {
        return -1;
      }
      return 0;
    })
    localStorage.setItem('agenda',JSON.stringify(this.events))
    this.nombreEncontrado.nombre = '';
    this.modal.dismissAll();
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event) => event !== eventToDelete);
    localStorage.setItem('agenda',JSON.stringify(this.events))
  }
  cancelarCita(event:any){
    this._agenda.eliminarCita(event.uid);
    this.deleteEvent(event);
    this.eventos2.splice(this.eventos2.indexOf(event),1)
    localStorage.setItem('agenda',JSON.stringify(this.eventos2))
    localStorage.setItem('agenda2',JSON.stringify(this.eventos2))
    this.modal.dismissAll();
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  eliminarLocal(){
    localStorage.removeItem('paciente')
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  // funcion para obtener pacientes
  obtenerDataPacientes(){
    this.afs.collection("Pacientes").ref.where("uid","==", this.doctor.uid).get()
    .then((query) =>{
      this.dataPacientes=[]
      query.forEach((doc) =>{
        let id = {
          idPaciente:doc.id
        }
        const returnedTarget = Object.assign(id, doc.data());
        this.dataPacientes.push(returnedTarget);
      })
    })
  }
  //fin obtener data pacientes

  //funcion para buscar los pacientes del doctor
  buscarCitas(event:any){
    this.citasEncontradas = [];
    if (event.target.value == '') {
      this.citasEncontradas = [];
      this.eventos2 = [];
      var day = startOfDay(new Date());
      this.dia = day.toDateString();
      let data = JSON.parse(localStorage.getItem('agenda')!)
      if(data != null){
        data.forEach((elemen:any) => {
          let evento = {
            title: elemen.title,
            start: new Date(elemen.start),
            end: new Date(elemen.end),
            color: elemen.color,
            draggable: elemen.draggable,
            resizable: {
              beforeStart: elemen.resizable.beforeStart,
              afterEnd: elemen.resizable.afterEnd,
            },
            motivo:elemen.motivo
          }
          if(evento.start.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' }) == this.day.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' })){
            this.eventos2 = [
              ...this.eventos2,
              evento
            ];
          }
        });
      }
    } else if (event.target.value.length >= 3){
      this.events.forEach((cita) => {
        if (cita.title.toLowerCase().includes(event.target.value.toLowerCase())) {
          this.citasEncontradas.push(cita)
          this.agregarConsulta(cita);
        }
      })
    }
  }
  //fin de la funcion

  agregarConsulta(event:any){
    this.eventos2 = []
    this.dia = event.start.toDateString()
    this.eventos2 = [
      ...this.eventos2,
      event
    ];
    this.citasEncontradas = [];
  }

  //funcion para buscar los pacientes del doctor
  buscarPaciente(event:any){
    this.obtenerDataPacientes();
    this.pacientesEncontrados = [];
    if (event.target.value == '') {
      this.pacientesEncontrados = [];
    } else if (event.target.value.length >= 3){
      this.dataPacientes.forEach((paciente) => {
        let nombreCompleto = paciente.nombre.concat(' ',paciente.apellidoPaterno,' ',paciente.apellidoMaterno);
        if (nombreCompleto.toLowerCase().includes(event.target.value.toLowerCase())) {
          this.pacientesEncontrados.push(paciente)
        }
      })
    }
  }
  //fin de la funcion

  agregarPaciente(event:any){
    this.paciente = event
    this.nombreEncontrado.nombre = this.paciente.nombre.concat(' ', this.paciente.apellidoPaterno, ' ',this.paciente.apellidoMaterno)
    this.pacientesEncontrados = [];
  }

  pasarCita(item:any){
    if (item.uid != undefined) {
      let data = JSON.parse(localStorage.getItem('agenda')!)
      data.forEach((element:any) => {
        if (element.uid == item.udi) {
          element.color = {
            primary: '#2ECC71',
            secondary: '#D5F5E3',
          }
        }
      });
      item.color = {
        primary: '#2ECC71',
        secondary: '#D5F5E3 ',
      }
      this._agenda.actualizarCita(item);
    }
  }

  cerrarSesion(){
  }
}
