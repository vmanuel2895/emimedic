import { Component, Input, OnInit } from '@angular/core';
import { FarmaciaService } from '../../services/farmacia/farmacia.service';
import { AngularFirestore} from '@angular/fire/firestore';
import { medico } from 'src/app/interfaces/medico';
import { Router } from '@angular/router';

@Component({
  selector: 'app-farmacia',
  templateUrl: './farmacia.component.html',
  styleUrls: ['./farmacia.component.scss']
})
export class FarmaciaComponent implements OnInit {

  public medicamentos:any[]=[];
  public user:medico = {} as medico;

  constructor(private stock:FarmaciaService,
              public afs: AngularFirestore,
              private router:Router) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user')!)
    this.obtenerMedicamentos();
  }

  obtenerMedicamentos(){
    this.medicamentos=[];
    this.afs.collection("Medicamentos").ref.where("uid","==", this.user.uid).get()
    .then((query) =>{
      query.forEach((doc) =>{
        let id = {
          idMedicamento:doc.id
        }
        this.medicamentos.push(Object.assign(id,doc.data()));
      })
      this.medicamentos.sort((a,b)=> a.medicamento.codigo - b.medicamento.codigo);
    });
  }

  eliminarMedicamento(id:any){
    this.stock.eliminarMedicamento(id)
    this.obtenerMedicamentos();
  }

  editarMedicamento(medicamento:any,id:any){
    localStorage.setItem('medicamento', JSON.stringify(Object.assign(medicamento,{idMedicamento:id})));
    this.router.navigate(['dashboard/agregar/medicamento']);
  }
}
