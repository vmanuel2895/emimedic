import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { LoginService } from 'src/app/services/login/login.service';
import { medico } from 'src/app/interfaces/medico';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-datos-facturacion',
  templateUrl: './datos-facturacion.component.html',
  styleUrls: ['./datos-facturacion.component.scss']
})
export class DatosFacturacionComponent implements OnInit {

  public user:medico = {} as medico;
  collection = { count: 0, data: [] }
  public botonEditar = false;
  public botonGuardar = true;
  public datos = {
    razonSocial:'',
    direccion:'',
    correoElectronico:'',
    rfc:'',
    telefono:'',
    conceptoFacturacion:''
  }
  public idDatos = '';

  constructor(public afs: AngularFirestore, public _login:LoginService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user')!);
    this.obtenerDatosFacturacion();
  }

  obtenerDatosFacturacion(){
    this.afs.collection("Facturacion").ref.where("uid","==", this.user.uid).get()
    .then((query) =>{
      query.forEach((doc) =>{
        this.botonEditar = true
        this.botonGuardar = false;
        this.idDatos = doc.id
        this.datos = doc.data().datos
      })
    });
  }


  agregar(form:NgForm){
    this._login.agregarDatosFacturacion(this.user,form.form.value);
  }

  actualizar(form:NgForm){
    this._login.actualizarDatosFacturacion(this.user, form.form.value, this.idDatos);
  }

}
