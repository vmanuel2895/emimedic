import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PacientesService } from "src/app/services/pacientes/pacientes.service";
import Swal from 'sweetalert2';
import { medico } from '../../interfaces/medico';
import { search, paciente } from '../../interfaces/paciente';
import { consulta } from '../../interfaces/consulta';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection} from '@angular/fire/firestore';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { historia } from '../../interfaces/historia-clinica';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.scss']
})
export class PacientesComponent implements OnInit {

  public user:medico = {} as medico;
  public docPacientes = [{
    uid: "",
    displayName: "",
    nombre : "",
    apellidoPaterno :"",
    apellidoMaterno: "",
    curp: "",
    edad: "",
    sexo:"",
    telefono: "",
    telEmergencia: "",
    calle: "",
    numeroInterior:"",
    numeroExterior:"",
    codigoPostal:"",
    colonia:"",
    municipio:"",
    estado:"",
    fechaInicio:""
  }];
  public dataPacientes : any []= []
  public searchPacientes:search = {} as search;
  public guardarSearch = true
  public editarVisible = true
  public datos ={}
  public consultas:any=[];
  @ViewChild('modalContent', { static: true }) modalContent?: TemplateRef<any>;
  @ViewChild('modalContent2', { static: true }) modalContent2?: TemplateRef<any>;
  @ViewChild('modalContent3', { static: true }) modalContent3?: TemplateRef<any>;
  @ViewChild('modalContent4', { static: true }) modalContent4?: TemplateRef<any>;
  public mostrarConsulta:consulta = {} as consulta;
  public nombrePacienteCompleto = '';

  public idDocumento = {idPaciente:""}
  public idDoc ="";
  public pacientesEncontrados:any [] = [];
  @ViewChild('filterName') fileInput!: ElementRef;
  public historiaClinicaCompleta = true;
  public historiaClinicaLlena:any [] = [];
  public verTituloHitoria = false;
  public datosHistoria:historia = {} as historia;
  public generoPaciente = '';
  public edadPaciente = 0;
  public generoMedico = '';
  public botonesImp = true;

  constructor(public service_paciente : PacientesService,
              public afs: AngularFirestore,
              private modal: NgbModal,
              private spinner: NgxSpinnerService){}

  ngOnInit(): void {
    // obtengo los datos del doctor
    this.user = JSON.parse(localStorage.getItem('user')!)
    this.generoMedico = JSON.parse(localStorage.getItem('user')!).genero
    //servicio para otener los pacientes del doctor
    this.service_paciente.obtenerDataPacientes();
    this.obtenerDataPacientes();
    this.searchPacientes = JSON.parse(localStorage.getItem('paciente')!);
    this.idDocumento = JSON.parse(localStorage.getItem('paciente')!)
    if(this.searchPacientes == undefined){
      this.guardarSearch=false
      this.verTituloHitoria = false;
      this.editarVisible
      this.historiaClinicaCompleta = true;
      this.historiaClinicaLlena = []
      this.searchPacientes = {
        apellidoMaterno: "",
        apellidoPaterno: "",
        calle: "",
        codigoPostal: "",
        colonia: "",
        curp: "",
        displayName: "",
        edad: "",
        estado: "",
        fechaInicio: "",
        municipio: "",
        nombre: "",
        numeroExterior: "",
        numeroInterior: "",
        sexo: "",
        telEmergencia: "",
        telefono: "",
        uid: "",
        idPaciente:""
      }
    }else {
      this.guardarSearch = true
      this.editarVisible =false
      this.historiaClinicaCompleta = false;
    }
    this.obtenerHistoricoConsultas()
    this.obtenerHistoriaClinica()
  }

   // validar curp
  validarCurp(form:NgForm){
    if(form.controls.curp.status == "INVALID" || form.controls.curp.value == ""){
      Swal.fire({
        position: 'top-end',
        icon: 'warning',
        title: 'CURP INVALIDO',
        text: 'Ingrese un CURP valido',
        showConfirmButton: false,
        timer: 1500
      })
      form.controls.curp.reset();
    }
  }//fin calidarCurp

  // funcion para agregar paciente
  savePaciente(form : NgForm){
    if (form.value.sexo != '' && form.value.edad) {
      this.service_paciente.setUserDataRegistro(this.user, form.value)
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'DATOS GUARDADOS CORRECTAMENTE',
        text: 'PACIENTE INGRESADO CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
      form.control.reset();
    }else{
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'INGRESA EL GENERO Y EDAD DEL PACIENTE',
        text: 'EL PACIENTE NO SE REGISTRO',
        showConfirmButton: false,
        timer: 1500
      })
    }
  }// fin savePaciente

  // funcion para obtener pacientes
  obtenerDataPacientes(){
    this.afs.collection("Pacientes").ref.where("uid","==", this.user.uid).get()
    .then((query) =>{
      this.dataPacientes=[]
      query.forEach((doc) =>{
        let id = {
          idPaciente:doc.id
        }
        const returnedTarget = Object.assign(id, doc.data());
        this.dataPacientes.push(returnedTarget);
      })
    })
  }
  //fin obtener data pacientes

  // funcion para obtener HC
  obtenerHistoricoConsultas(){
    this.consultas=[]
    this.afs.collection("HistoriaClinica").ref.where("uid","==", this.user.uid).get()
    .then((query) =>{
      query.forEach((doc) =>{
        this.datos = doc.data();
        if(doc.data().idPaciente == this.searchPacientes.idPaciente){
          this.consultas.push(this.datos)
        }
      })
      this.ordenarConsultasFecha(this.consultas);
    })
  }

  ordenarConsultasFecha(datos:any){
    datos.sort(function(a:any, b:any) { a = new Date(a.fechaInicio); b = new Date(b.fechaInicio); return a>b ? -1 : a<b ? 1 : 0; });
  }

  // funcion para obtener HCC
  obtenerHistoriaClinica(){
    this.historiaClinicaLlena=[]
    this.afs.collection("HistoriaClinicaCompleta").ref.where("uid","==", this.user.uid).get()
    .then((query) =>{
      query.forEach((doc) =>{
        if(doc.data().idPaciente == this.searchPacientes.idPaciente){
          this.generoPaciente = doc.data().paciente.sexo
          this.edadPaciente = doc.data().paciente.edad
          this.historiaClinicaLlena.push(doc.data());
          if (this.historiaClinicaLlena.length != 0) {
            this.historiaClinicaCompleta = true;
            this.verTituloHitoria = true;
          }
        }
      })
    })
  }

  /* else{
          if (this.searchPacientes.nombre == '') {
            console.log('entro3');
            this.historiaClinicaCompleta = true;
            this.verTituloHitoria = false;
          }else{
            console.log('entro4');
            this.historiaClinicaCompleta = false;
            this.verTituloHitoria = false;
          }
        } */

  //funcion para Actualizar Paciente
  actualizaDatos (form : NgForm) {
    const doc = this.searchPacientes.idPaciente
    const nombre = form.controls.nombre.value
    const apellidoPaterno = form.controls.apellidoPaterno.value
    const apellidoMaterno = form.controls.apellidoMaterno.value
    const curp=  form.controls.curp.value
    const edad=  form.controls.edad.value
    const sexo= form.controls.sexo.value
    const telefono= form.controls.telefono.value
    const telEmergencia= form.controls.telEmergencia.value
    const calle= form.controls.calle.value
    const numeroInterior= form.controls.numeroInterior.value
    const numeroExterior= form.controls.numeroExterior.value
    const codigoPostal= form.controls.codigoPostal.value
    const colonia= form.controls.colonia.value
    const municipio= form.controls.municipio.value
    const estado= form.controls.estado.value

    this.afs.collection("Pacientes").doc(doc).update({
      nombre : nombre ,
      apellidoPaterno : apellidoPaterno ,
      apellidoMaterno: apellidoMaterno,
      curp: curp,
      edad: edad,
      sexo: sexo,
      telefono: telefono,
      telEmergencia: telEmergencia,
      calle: calle,
      numeroInterior: numeroInterior,
      numeroExterior: numeroExterior,
      codigoPostal: codigoPostal,
      colonia: colonia,
      municipio: municipio,
      estado: estado,
    }).then(()=>{
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'DATOS ACTUALIZADOS CORRECTAMENTE',
        text: 'PACIENTE ACTUALIZADO CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
    })
  }

  //funcion para buscar los pacientes del doctor
  buscarPaciente(event:any){
    this.obtenerDataPacientes();
    this.pacientesEncontrados = [];
    if (event.target.value == '') {
      this.pacientesEncontrados = [];
    }else if (event.target.value.length >= 3){
      this.dataPacientes.forEach((paciente) => {
        let nombreCompleto = paciente.nombre.concat(' ',paciente.apellidoPaterno,' ',paciente.apellidoMaterno);
        if (nombreCompleto.toLowerCase().includes(event.target.value.toLowerCase())) {
          this.pacientesEncontrados.push(paciente)
        }
      })
    }
  }
  //fin de la funcion

  //funcion para agregar al paciente al local storage
  async agregarPaciente(paciente:any){
    this.pacientesEncontrados = [];
    this.fileInput.nativeElement.value = '';
    this.searchPacientes=paciente;
    await localStorage.setItem('paciente', JSON.stringify(paciente))
    this.obtenerHistoricoConsultas();
    this.obtenerHistoriaClinica();
    this.guardarSearch = true
    this.editarVisible =false
    this.historiaClinicaCompleta = false;
    this.botonesImp = false
  }
  //fin de la funcion

  mostrarModal(item:any){
    this.mostrarConsulta = item
    this.mostrarConsulta.edad = item.edad.concat(' años')
    this.mostrarConsulta.peso = item.peso.toString().concat(' Kg')
    this.mostrarConsulta.talla = item.talla.toString().concat(' Mts')
    this.mostrarConsulta.temp = item.temp.toString().concat(' °C')
    this.mostrarConsulta.ta = item.ta.toString().concat(' mmHg')
    this.mostrarConsulta.imc = item.imc.toString().concat(' Kg/m²')
    this.mostrarConsulta.alergias = item.alergias.toString()
    this.nombrePacienteCompleto = this.mostrarConsulta.paciente.nombre.concat(' ', this.mostrarConsulta.paciente.apellidoPaterno,' ',this.mostrarConsulta.paciente.apellidoMaterno)
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  siguiente(nombre:string){
    if(nombre == 'NOTA'){
      this.modal.dismissAll();
      this.modal.open(this.modalContent2, { size: 'lg' });
    }else if(nombre == 'RECETA'){
      this.modal.dismissAll();
      this.modal.open(this.modalContent3, { size: 'lg' });
    }
  }

  anterior(nombre:string){
    if(nombre == 'RECETA'){
      this.modal.dismissAll();
      this.modal.open(this.modalContent, { size: 'lg' });
    }else if(nombre == 'ESTUDIOS'){
      this.modal.dismissAll();
      this.modal.open(this.modalContent2, { size: 'lg' });
    }
  }

  mostrarModalHistoria(item:any){
    this.datosHistoria = item.historiaClinica
    this.modal.open(this.modalContent4, { size: 'lg' });
  }

///////////////////////// pdfs
spinerConsentimiento(){
  this.spinner.show();
  setTimeout(() => {
    this.consentimientoPDF();
  }, 1000);
}

consentimientoPDF(){
  var fechas =  Date.now()
  const hoy = new Date(fechas)
  let photoInst = JSON.parse(localStorage.getItem('user')!).photoIntitucion

  const doc : any = new jsPDF()
  doc.setTextColor(100);
  doc.setFont('courier')
  doc.setFontSize(20);
  doc.text("CONSENTIMIENTO INFORMADO",50,20)
  doc.setFontSize(12);
  var img = new Image()
  img.src = 'assets/images/encabezado.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img, 'png', 10, 10, 10, 20)

  var img2 = new Image()
  img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img2, 'png', 5, 30, 210, 3)

  if(photoInst != undefined){
    var fotoInstitu = new Image();
    fotoInstitu.src = photoInst
    doc.addImage(fotoInstitu, 'JPEG' , 175, 7, 22, 22)
  }

  doc.text("Fecha: " + hoy.toLocaleDateString(undefined,{year:'numeric',month:'long',day:'numeric'}),10,40)
  doc.text(" Hora: "+hoy.toLocaleTimeString('en-US'),150,40)
  doc.text("Yo(Paciente): "+ this.searchPacientes.nombre +" "+ this.searchPacientes.apellidoPaterno +" "+ this.searchPacientes.apellidoMaterno,10,50)
  doc.text("Representante legal del paciente: ",10,60)
  doc.text("Parentesco: ",130,60)
  doc.text("Identificado con: ",10,70)
  doc.text("_____________________________",55,70)
  doc.text("Número: ",130,70)
  doc.text("_____________",145,70)

  doc.text("Declaro que: ",10,80)
  doc.text("1. Me han informado clara y ampliamente el medico tratante,",10,85)
  doc.text("doctor (a): " +this.user.nombre +" "+ this.user.apellidoPaterno +" " +this.user.apellidoMaterno,10,90)
  doc.text("de los problemas de salud que tengo ( tiene mi enfermo), ",10,95)
  doc.text("y que estos pueden traerme(le) consecuencias si no son tratados,",10,100)
  doc.text("así como la naturaleza de mi ( su ) salud, su evolución natural, tratamientos ",10,105)
  doc.text("y los procedimientos recomendados.",10,110)

  doc.text("2.Se me han explicado las contraindicaciones o consecuencias negativas del",10,120)
  doc.text("tratamiento propuesto, así como las consecuencias negativas de no aceptar ",10,125)
  doc.text("o iniciar el tratamiento a tiempo.",10,130)

  doc.text("3. Me han explicado cuales son los medicamentos y/o materiales requeridos ",10,140)
  doc.text("para el tratamiento/procedimiento.",10,145)

  doc.text("4. Me han explicado claramente los riesgos de cicatrización, infección,",10,155)
  doc.text("inflamación o intolerancia del tratamiento/procedimiento , asi como",10,160)
  doc.text("las medidas a seguir en caso de infección.",10,165)

  doc.text("5. Me ha sido explicado de manera satisfactoria y en lenguaje comprensible",10,175)
  doc.text(" la información referente a la enfermedad / padecimiento a tratar  corres-",10,180)
  doc.text("pondiente al diagnóstico y sus posibles complicaciones.",10,185)

  doc.text("6. He formulado todas las preguntas que consideré necesarias sobre ",10,195)
  doc.text("los aspectos antes mencionados y estoy satisfecho con las respuestas ",10,200)
  doc.text("obtenidas del médico tratante.",10,205)

  doc.text("7. Se me ha explicado y he comprendido los beneficios y alternativas del ",10,215)
  doc.text("procedimiento/tratamiento.",10,220)

  doc.text("8. Se me ha explicado, que aún cumpliendo con las indicaciones recibidas y/o ",10,230)
  doc.text("la medicación indicada, existe el riesgo de aparición de sintomas secundarios,",10,235)
  doc.text(" lo cual no es de ninguna forma responsabilidad del médico tratante o del ",10,240)
  doc.text("consultorio, en donde se me presta el servicio, lo cual acepto.",10,245)

  var img3 = new Image()
  img3.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img3, 'png', 5, 280, 210, 3)


  doc.addPage()

  var img4 = new Image()
  img4.src = 'assets/images/encabezado.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img4, 'png', 10, 10, 10, 20)

  var img5 = new Image()
  img5.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img5, 'png', 5, 30, 210, 3)

  if(photoInst != undefined){
    var fotoInstitu = new Image();
    fotoInstitu.src = photoInst
    doc.addImage(fotoInstitu, 'JPEG' , 175, 7, 22, 22)
  }

  doc.text("Habiendo leído y entendido claramente los términos  que se me ( nos ) han ",10,40)
  doc.text("expuesto, solicito y autorizo al médico tratante , Doctor (a)",10,45)
  doc.text(this.user.nombre +" "+ this.user.apellidoPaterno +" " +this.user.apellidoMaterno + "  ha realizar el siguiente procedimiento",10,50)

  doc.text("________" + " Aplicación de inyecciones",10,70)
  doc.text("________" + " Lavado de Oidos",10,80)
  doc.text("________" + " Retiro de puntos",10,90)
  doc.text("________" + " Otros: " + "___________________________________________",10,100)

  doc.text("Sin perjuicio  de los procedimientos que se realizarán, declaro, tener plena",10,120)
  doc.text(" capacidad, conciencia y lucidez, para decidir y aceptar el procedimiento  ",10,125)
  doc.text("arriba mencionado, bajo mi completa y absoluta responsabilidad",10,130)

  doc.text("Observaciones: ",10,140)

  doc.text("Nombre y firma del paciente",100,190)
  doc.text( this.searchPacientes.nombre +" "+ this.searchPacientes.apellidoPaterno +" "+ this.searchPacientes.apellidoMaterno,100,195)
  doc.text("_____________________________________",100,200)

  doc.text("Nombre y firma del representante Legal",100,210)
  doc.text("______________________________________",100,215)

  doc.text("Nombre y firma del medico tratante",100,225)
  doc.text(this.user.nombre +" "+ this.user.apellidoPaterno +" " +this.user.apellidoMaterno,100,230)
  doc.text("______________________________________",100,235)
  doc.text("Cédula: "+ this.user.cedula,100,240)

  var img6 = new Image()
  img6.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img6, 'png', 5, 280, 210, 3)
  setTimeout(() => {
    /** spinner ends after 5 seconds */
    this.spinner.hide();
  }, 5000);
  doc.save('CONSENTIMIENTO INFORMADO')
}

spinerCertificado(){
  this.spinner.show();
  setTimeout(() => {
    this.certificadoPDF();
  }, 1000);
}

certificadoPDF(){
  let photoInst = JSON.parse(localStorage.getItem('user')!).photoIntitucion
  var fechas =  Date.now()
  const hoy = new Date(fechas)
  const doc : any = new jsPDF()
  doc.setTextColor(100);
  doc.setFont('courier')
  doc.setFontSize(20);
  doc.text("CERTIFICADO MÉDICO",60,20)
  doc.setFontSize(12);
  var img = new Image()
  img.src = 'assets/images/encabezado.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img, 'png', 10, 10, 10, 20)

  var img2 = new Image()
  img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img2, 'png', 5, 30, 210, 3)

  if(photoInst != undefined){
    var fotoInstitu = new Image();
    fotoInstitu.src = photoInst
    doc.addImage(fotoInstitu, 'JPEG' , 175, 7, 22, 22)
  }

  doc.text("Fecha: " + hoy.toLocaleDateString(undefined,{year:'numeric',month:'long',day:'numeric'}),10,40)
  doc.text(" Hora: "+hoy.toLocaleTimeString('en-US'),150,40)

  doc.text("EL que suscribe " + this.user.nombre +" "+ this.user.apellidoPaterno +" " + this.user.apellidoMaterno +", " +"Médico cirujano legalmente",10,70)
  doc.text("autorizado para el ejercicio de la profesion con cédula:"+ this.user.cedula,10,75)

  doc.text("CERTIFICA",90,95)

  doc.text("Haber examinado a: "+ this.searchPacientes.nombre + " "+ this.searchPacientes.apellidoPaterno +" "+this.searchPacientes.apellidoMaterno + " de " +this.searchPacientes.edad +" años de edad",10,105)
  doc.text("a quien por interrogatorio y exploración física se le detecto: ",10,110)
  doc.text("___________________________________________",40,125)

  doc.text("Por lo que se puede realizar cualquier actividad sin poner en riesgo su",10,135)
  doc.text("estado actual de salud",10,140)

  doc.text("TA: _______ FC: ______ FR: ______",50,150)
  doc.text("TEMP: _______  PESO: _______ TALLA: ______",40,155)
  doc.text("ALERGIAS: _______  GRUPO SANGUINEO: _______ RH: _______",30,160)

  doc.text("se extiende el presente certificado para los fines que al interesado ",10,180)
  doc.text("convengan, en el municipio de ___________________________________",10,185)

  doc.text( this.user.nombre +" "+ this.user.apellidoPaterno +" " + this.user.apellidoMaterno,70,240)
  doc.text("_______________________________________",55,245)
  doc.text("Nombre y firma del médico",75,250)

  var img3 = new Image()
  img3.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img3, 'png', 5, 270, 210, 3)
  setTimeout(() => {
    /** spinner ends after 5 seconds */
    this.spinner.hide();
  }, 3000);
  doc.save('CERTIFICADO MEDICO')
}

spinerReceta(){
  this.spinner.show();
  setTimeout(() => {
    this.recetaPDF();
  }, 1000);
}

recetaPDF(){
  let photoInst = JSON.parse(localStorage.getItem('user')!).photoIntitucion
  var fechas =  Date.now()
  const hoy = new Date(fechas)

  const doc : any = new jsPDF()
  doc.setTextColor(100);
  doc.setFont('arial')
  doc.setFontSize(10);

  var img = new Image()
  img.src = 'assets/images/encabezado.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img, 'png', 10, 10, 20, 40)

  var img2 = new Image()
  img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img2, 'png', 5, 50, 210, 3)

  var img3 = new Image()
  img3.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img3, 'png', 5, 250, 210, 3)

  var img4 = new Image()
  img4.src = 'assets/images/fondomedic.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img4, 'png', 70, 80, 70, 100)

  if(photoInst != undefined){
    var fotoInstitu = new Image();
    fotoInstitu.src = photoInst
    doc.addImage(fotoInstitu, 'JPEG' , 165, 5, 40, 40)
  }

  if (this.generoMedico == 'MASCULINO') {
    doc.text("DR." + this.user.nombre +" "+ this.user.apellidoPaterno +" " + this.user.apellidoMaterno,60,20)
  }else{
    doc.text("DRA." + this.user.nombre +" "+ this.user.apellidoPaterno +" " + this.user.apellidoMaterno,60,20)
  }
  doc.text(this.user.universidad,60,25)
  doc.text("CÉDULA PROFESIONAL: "+this.user.cedula,60,30)
  doc.text(this.user.titulo,60,35)
  doc.text("Fecha: " + hoy.toLocaleDateString(undefined,{year:'numeric',month:'long',day:'numeric'}),10,70)
  doc.text(" Hora: "+hoy.toLocaleTimeString('en-US'),170,70)

  doc.text("Edad: "+ this.mostrarConsulta.edad,170,90)
  doc.text("Peso: "+this.mostrarConsulta.peso,170,100)
  doc.text("Talla: "+this.mostrarConsulta.talla,170,110)
  doc.text("Ta: "+this.mostrarConsulta.ta,170,120)
  doc.text("IMC: "+this.mostrarConsulta.imc,170,130)
  doc.text("Temp: "+this.mostrarConsulta.temp,170,140)
  doc.text("Alergias: " + this.mostrarConsulta.alergias,170,150)
  doc.text("Paciente: " + this.mostrarConsulta.paciente.nombre +" "+ this.mostrarConsulta.paciente.apellidoPaterno+" "+this.mostrarConsulta.paciente.apellidoMaterno,10,90)
  doc.text("Dx: " + this.mostrarConsulta.nota,10,100,{align: 'justify',lineHeightFactor: 1.5,maxWidth:148})

  // doc.text("Nombre y firma del medico tratante",100,125)
  // doc.text(this.user.nombre +" "+ this.user.apellidoPaterno +" " +this.user.apellidoMaterno,100,130)
   doc.text("______________________________________",130,240)
   doc.text("FIRMA",160,245)

   doc.text(this.user.consultorio +" "+ this.user.numeroExterior +" "+ this.user.colonia +" C.P: "+this.user.codigoPostal +", " +this.user.municipio+", "+this.user.estado,15,270)
   doc.text("PREVIA CITA TELEFÓNICA",15,275)
   doc.text("TEL: " +this.user.telefono,15,280)
  // doc.text("Cédula: "+ this.user.cedula,100,140)

  // doc.output('dataurlnewwindow')
  setTimeout(() => {
    /** spinner ends after 5 seconds */
    this.spinner.hide();
  }, 3000);
  doc.save("RECETA MEDICA")
}

spinerHistoria(){
  this.spinner.show()
  setTimeout(() => {
    this.historiaclinicaPDF();
  }, 1000);
}

historiaclinicaPDF(){
  let photoInst = JSON.parse(localStorage.getItem('user')!).photoIntitucion

  const doc : any = new jsPDF()
  doc.setTextColor(100);
  doc.setFont('arial')
  doc.setFontSize(24);

  var img = new Image()
  img.src = 'assets/images/encabezado.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img, 'png', 10, 10, 20, 40)

  var img2 = new Image()
  img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img2, 'png', 0, 50, 210, 3)

  if(photoInst != undefined){
    var fotoInstitu = new Image();
    fotoInstitu.src = photoInst
    doc.addImage(fotoInstitu, 'JPEG' , 165, 5, 40, 40)
  }

  /* var img3 = new Image()
  img3.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img3, 'png', 5, 250, 210, 3) */

  /* var img4 = new Image()
  img4.src = 'assets/images/fondomedic.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img4, 'png', 70, 80, 70, 100) */

  doc.text("HISTORIA CLÍNICA",60,30)
  doc.setFontSize(12);
  doc.text("PACIENTE: " + this.searchPacientes.nombre +" "+ this.searchPacientes.apellidoPaterno+" "+this.searchPacientes.apellidoMaterno,10,60)
  if (this.generoMedico == 'MASCULINO') {
    doc.text("DR." + this.user.nombre +" "+ this.user.apellidoPaterno +" " + this.user.apellidoMaterno,120,60)
  }else{
    doc.text("DRA." + this.user.nombre +" "+ this.user.apellidoPaterno +" " + this.user.apellidoMaterno,120,60)
  }
  // doc.text(this.user.universidad,120,35)
  doc.text("CÉDULA PROFESIONAL "+this.user.cedula,120,65).setFont(undefined, 'bold');

  var img2 = new Image()
  img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img2, 'png', 0, 70, 210, 3)

  doc.text("ANTECEDENTES HEREDO FAMILIARES",60,85).setFont(undefined, 'normal');

  doc.text("DIABETES: "+ this.datosHistoria.diabetes,10,95)
  doc.text("HIPERTENSIÓN:  "+this.datosHistoria.hipertencion,120,95)
  doc.text("CÁNCER:  "+this.datosHistoria.cancer,10,105)
  doc.text("TIPO:  "+this.datosHistoria.tipoHipertencion,120,105)
  doc.text("CARDIOPATÍAS: "+this.datosHistoria.cardiopatias,10,115)
  doc.text("NEFROPATÍAS:  "+this.datosHistoria.nefropatas,120,115)
  doc.text("MALFORMACIONES:  "+this.datosHistoria.malformaciones,10,125)
  doc.text("TIPO:  "+this.datosHistoria.tipoNefropatas,120,125)
  doc.text("OTROS:  "+this.datosHistoria.otros,10,135).setFont(undefined, 'bold');

  var img2 = new Image()
  img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img2, 'png', 0, 150, 210, 3)

  doc.text("ANTECEDENTES PATOLÓGICOS",60,170).setFont(undefined, 'normal');

  doc.text("ENFERMEDADES DE LA INFANCIA:  "+this.datosHistoria.enfermadesInfancia,10,180)
  doc.text("SECUELAS:  "+this.datosHistoria.secuelas,120,180)
  doc.text("HOSPITALIZACIONES PREVIAS:  "+this.datosHistoria.hospitalizacionPrevia,10,190)
  doc.text("ESPECIFICAR :  "+this.datosHistoria.especificarHP,120,190)
  doc.text("ANTECEDENTES QUIRÚRGICOS:  "+this.datosHistoria.atecedentesQuirurgicos,10,200)
  doc.text("ESPECIFICAR :  "+this.datosHistoria.especificarAQ,120,200)
  doc.text("TRANSFUSIONES PREVIAS:  "+this.datosHistoria.transfusionesPrevias,10,210)
  doc.text("ESPECIFICAR: "+this.datosHistoria.especificarTP,120,210)
  doc.text("FRACTURAS: "+this.datosHistoria.fracturas,10,220)
  doc.text("ACCIDENTES/TRAUMATISMOS: "+this.datosHistoria.acidenteTraumatismo,120,220)
  doc.text("OTRA ENFERMEDAD: "+this.datosHistoria.otraEnfermedad,10,230)
  doc.text("ESPECIFICAR: "+this.datosHistoria.especificarOE,120,230)
  doc.text("MEDICAMENTOS ACTUALES: "+this.datosHistoria.medicamentosActuales,10,240)
  doc.text("ESPECIFICAR:  "+this.datosHistoria.especificarMA,120,240).setFont(undefined, 'bold');

  var img2 = new Image()
  img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img2, 'png', 0, 250, 210, 3)

  doc.addPage();

  doc.text("ANTECEDENTES NO PATOLÓGICOS",60,20)
  doc.text("TABAQUISMO:",10,30).setFont(undefined, 'normal');
  doc.text("¿CUÁNTOS POR DÍA?: "+ this.datosHistoria.tabaquismo,10,40)
  doc.text("AÑOS DE CONSUMO:  "+ this.datosHistoria.aniosConsumoCigarro,10,45)
  doc.text("EXFUMADOR O TABAQUISMO PASIVO:  "+ this.datosHistoria.exfumador,10,50).setFont(undefined, 'bold');
  doc.text("ALCOHOLISMO:",10,60).setFont(undefined, 'normal');
  doc.text("¿CUÁNTAS COPAS POR SEMANA?: "+ this.datosHistoria.alcoholismo,10,70)
  doc.text("AÑOS DE CONSUMO:  "+ this.datosHistoria.aniosConsumoAlcohol,10,75)
  doc.text("EXALCOHÓLICO Y/U OCASIONAL:  "+ this.datosHistoria.exalcoholico,10,80).setFont(undefined, 'bold');
  doc.text("DROGADICCIÓN:",10,90).setFont(undefined, 'normal');
  doc.text("TIPO: "+ this.datosHistoria.drogadiccion,10,95)
  doc.text("AÑOS DE CONSUMO:  "+ this.datosHistoria.aniosConsumoDrogas,60,95)
  doc.text("EXDROGADICTO:  "+ this.datosHistoria.exdrogradicto,120,95)
  doc.text("ALERGIAS:  "+ this.datosHistoria.alergias,10,105)
  doc.text("TIPO:  "+ this.datosHistoria.tipoAlergia,60,105)
  doc.text("ALIMENTACIÓN ADECUADA:  "+ this.datosHistoria.alimentacion,10,115)
  doc.text("VIVIENDA CON SERVICIOS BÁSICOS:  "+ this.datosHistoria.vivienda,120,115)
  doc.text("TIPO SANGUÍNEO:  "+ this.datosHistoria.tipoSanguineo,10,125).setFont(undefined, 'bold');

  var img2 = new Image()
  img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
  doc.addImage(img2, 'png', 0, 135, 210, 3)



  if(this.generoPaciente != 'Masculino' && this.edadPaciente > 12){
    /* doc.addPage();*/
    doc.text("ANTECENDENTES GINECOBSTÉTRICOS",60,145).setFont(undefined, 'normal');
    doc.text("¿EMBARAZO ACTUALMENTE?:  "+this.datosHistoria.embarazada,10,155)
    doc.text("¿ENBARAZADA DE ALTO RIESGO?:  "+this.datosHistoria.embarazoRiesgo,90,155)
    doc.text("ADMINISTRACIÓN:",10,165)
    doc.text("HIERRO: "+this.datosHistoria.hierro,10,170)
    doc.text("ÁCIDO FÓLICO: "+this.datosHistoria.acidoFolico,50,170)
    doc.text("FECHA DE ADMINISTRACIÓN:  "+this.datosHistoria.fechaAdministracion,90,170)
    doc.text("¿MÉTODO ANTICONCEPTIVO DESPUÉS DEL PARTO?: "+this.datosHistoria.metodoAnticonceptivo,10,180)
    doc.text("INICIO DE LA VIDA SEXUAL ACTIVA: "+this.datosHistoria.inicioVidaSexual,10,190)
    doc.text("FECHA DEL ÚLTIMO PARTO:  "+this.datosHistoria.fechaUltimoParto,10,200)
    doc.text("CRECIMIENTO DE VELLO PUBICO:  "+this.datosHistoria.crecimientoVellosPubicos,10,210)
    doc.text("TIPO DE MÉTODO DE PLANIFICACIÓN:  "+this.datosHistoria.metodoPlanif,10,220)
    doc.text("GESTAS: "+this.datosHistoria.gestas+ "          PARTOS: "+this.datosHistoria.partos+ "          CESÁREAS: "+this.datosHistoria.cesareas+ "          ABORTOS: "+this.datosHistoria.abortos,10,230)
    doc.text("ÚLTIMO PAPANICOLAO: "+this.datosHistoria.ultimoPapanicola,10,240)
    doc.text("ÚLTIMA COLPOSCOPÍA:  "+this.datosHistoria.ultimaColposcopia,100,240)
    doc.text("ÚLTIMA REVISIÓN MAMARIA: "+this.datosHistoria.ultimaRevisionMamaria,10,250)
    doc.text("NACIDOS VIVOS: "+this.datosHistoria.nacidosVivos,10,260)
    doc.text("NACIDOS MUERTOS: "+this.datosHistoria.nacidosMuertos,55,260)
    doc.text("PRIMERA MENSTRUACIÓN:  "+this.datosHistoria.primeraMenstruasion,10,270)
    doc.text("CRECIMIENTO DE PECHOS:  "+this.datosHistoria.crecimientoPechos,100,270)
    doc.text("PRIMER EMBARAZO:  "+this.datosHistoria.primerEmbarazo,10,280)
    doc.text("FECHA DEL PRIMER EMBARAZO:  "+this.datosHistoria.primerEmbarazoFecha,100,280)
    var img2 = new Image()
    img2.src = 'assets/images/linea.png'  //  (img,'png', desde donde inicia, que tan abajo, largo, ancho )
    doc.addImage(img2, 'png', 0, 290, 210, 3)
  }

  setTimeout(() => {
    /** spinner ends after 5 seconds */
    this.spinner.hide();
  }, 3000);
  doc.save("HISTORIA CLINICA")
  /* doc.output('dataurlnewwindow') */
}

}

