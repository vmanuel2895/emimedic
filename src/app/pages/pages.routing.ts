import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../guards/auth.guard';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PacientesComponent } from './pacientes/pacientes.component';
import { HistoriaClinicaComponent } from './historia-clinica/historia-clinica.component';
import { BitacoraComponent } from './bitacora/bitacora.component';
import { HistoriaClinicaCompletaComponent } from './historia-clinica-completa/historia-clinica-completa.component';
import { PerfilComponent } from './perfil/perfil.component';
import { FarmaciaComponent } from './farmacia/farmacia.component';
import { AgregarMedicamentoComponent } from './agregar-medicamento/agregar-medicamento.component';
import { DatosFacturacionComponent } from './datos-facturacion/datos-facturacion.component';
import { ActivarDoctoresComponent } from './activar-doctores/activar-doctores.component';

const routes: Routes = [
    {
        path: 'dashboard',
        component: PagesComponent,
        canActivate: [ AuthGuard ],
        children: [
            { path: '', component: DashboardComponent, data:{titulo : 'EMIMEDIC'} },
            /* { path: 'dashboard', component: DashboardComponent, data:{titulo : 'EMIMEDIC'} }, */
            { path: 'pacientes', component: PacientesComponent, data:{titulo : 'EMIMEDIC'} },
            { path: 'historia/clinica', component: HistoriaClinicaComponent, data:{titulo : 'EMIMEDIC'} },
            { path: 'bitacora/pacientes', component: BitacoraComponent, data:{titulo : 'EMIMEDIC'} },
            { path: 'historia/clinica/completa', component: HistoriaClinicaCompletaComponent, data:{titulo : 'EMIMEDIC'} },
            { path: 'perfil', component: PerfilComponent, data:{titulo : 'EMIMEDIC'} },
            { path: 'medicamentos', component: FarmaciaComponent, data:{titulo : 'EMIMEDIC'} },
            { path: 'agregar/medicamento', component: AgregarMedicamentoComponent, data:{titulo : 'EMIMEDIC'} },
            { path: 'agregar/datos/facturacion', component: DatosFacturacionComponent, data:{titulo : 'EMIMEDIC'} },
            { path: 'activar/medicos', component: ActivarDoctoresComponent, data:{titulo : 'EMIMEDIC'} }
        ]
    },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class PagesRoutingModule {}
