export interface medicamento{
  cantidad:number,
  laboratorio:string,
  lote:string,
  fechaCaducidad:Date,
  dosis:string,
  formaFarmaceutica:string,
  nombreComercial:string,
  nombreCientifico:string
}
