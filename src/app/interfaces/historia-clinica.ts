export interface historia{
  acidenteTraumatismo: string
  alcoholismo: number
  alergias: string
  alimentacion: string
  atecedentesQuirurgicos: string
  aniosConsumoAlcohol: string
  aniosConsumoCigarro: string
  aniosConsumoDrogas: string
  cancer: string
  cardiopatias: string
  cualAT: string
  diabetes: string
  drogadiccion: string
  enfermadesInfancia: string
  especificarAQ: string
  especificarHP: string
  especificarMA: string
  especificarOE: string
  especificarTP: string
  exalcoholico: string
  exdrogradicto: string
  exfumador: string
  fracturas: string
  hipertencion: string
  hospitalizacionPrevia: string
  malformaciones: string
  medicamentosActuales: string
  nefropatas: string
  otraEnfermedad: string
  otros: string
  secuelas: string
  tabaquismo: number
  tipoAlergia: string
  tipoHipertencion: string
  tipoNefropatas: string
  tipoSanguineo: string
  transfusionesPrevias: string
  vivienda: string

  embarazada:string,
  embarazoRiesgo:string,
  hierro:number,
  acidoFolico:number,
  fechaAdministracion:string,
  metodoAnticonceptivo:string,
  inicioVidaSexual:string,
  fechaUltimoParto:string,
  crecimientoVellosPubicos:string,
  metodoPlanif:string,
  gestas:number,
  partos:number,
  cesareas:number,
  abortos:number,
  ultimoPapanicola:string,
  ultimaColposcopia:string,
  ultimaRevisionMamaria:string,
  nacidosVivos:number,
  nacidosMuertos:number,
  primeraMenstruasion:string,
  crecimientoPechos:string,
  primerEmbarazo:string,
  primerEmbarazoFecha:string
  /* //si el caso es mujer
embarazoacutal:string
embarazoaltoriesgo:string
gesta:string

cesarea:string

adminhierro:string
adminacidofolico:string
ultimopapanicolao:string
ultimocolposcopia:string
ultimarevisionmamaria:string
fechaadmin:string
nacidosvivos:string
nacidosmuertos:string
metodoantidespuesparto:string
iniciovidasexual:string
fechaultimoparto:string
crecimientovellospublicos:string
tipometodoplanificacion:string
primermenstruacion:string
crecimientopechos:string
primerembarazo:string
fechaprimerembarazo:string */

}

export interface datosPNP{
  tabaquismo:number,
  alcoholismo:number,
  alergias:string,
  alimentacion:string,
  drogadiccion:string,
  aniosConsumoCigarro:string,
  aniosConsumoAlcohol:string,
  tipoAlergia:string,
  aniosConsumoDrogas:string,
  exfumador:string,
  exalcoholico:string,
  tipoSanguineo:string,
  vivienda:string,
  exdrogradicto:string,
}

export interface datosPP{
  enfermadesInfancia:string,
  hospitalizacionPrevia:string,
  atecedentesQuirurgicos:string,
  transfusionesPrevias:string,
  fracturas:string,
  otraEnfermedad:string,
  medicamentosActuales:string,
  secuelas:string,
  especificarHP:string,
  especificarAQ:string,
  especificarTP:string,
  acidenteTraumatismo:string,
  cualAT:string,
  especificarOE:string,
  especificarMA:string
}

export interface datosGineco{
  embarazada:string,
  embarazoRiesgo:string,
  hierro:number,
  acidoFolico:number,
  fechaAdministracion:string,
  metodoAnticonceptivo:string,
  inicioVidaSexual:string,
  fechaUltimoParto:string,
  crecimientoVellosPubicos:string,
  metodoPlanif:string,
  gestas:number,
  partos:number,
  cesareas:number,
  abortos:number,
  ultimoPapanicola:string,
  ultimaColposcopia:string,
  ultimaRevisionMamaria:string,
  nacidosVivos:number,
  nacidosMuertos:number,
  primeraMenstruasion:string,
  crecimientoPechos:string,
  primerEmbarazo:string,
  primerEmbarazoFecha:string
}

export interface heredo{
  diabetes:string,
  cancer:string,
  cardiopatias:string,
  malformaciones:string,
  otros:string,
  hipertencion:string,
  tipoHipertencion:string,
  nefropatas:string,
  tipoNefropatas:string,
}
