export interface consulta{
  alergias: ""
  cie10: [{
    CATALOG_KEY: ""
    CONSECUTIVO: ""
    LETRA: ""
    NOMBRE: ""
    id: ""
  }]
  displayName: ""
  edad: ""
  estudios: ""
  fechaInicio: ""
  idPaciente: ""
  nota: ""
  paciente: {
    activo: false
    apellidoMaterno: ""
    apellidoPaterno: ""
    calle: ""
    codigoPostal: ""
    colonia: ""
    curp: ""
    displayName: ""
    edad: ""
    estado: ""
    fechaInicio: ""
    idPaciente: ""
    municipio: ""
    nombre: ""
    numeroExterior: ""
    numeroInterior: ""
    sexo: ""
    telEmergencia: ""
    telefono: ""
    uid: ""
  }
  peso: 0
  ta: 0
  talla: 0
  temp: 0,
  imc: 0,
  uid: ""
}
