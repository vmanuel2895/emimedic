import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class AgendaService {
  public array:any[]=[];
  public array2:any[]=[];
  constructor(private router:Router,
              public afs: AngularFirestore) {
              }

  // funcion para agregar la historia clinica al paciente
  agendar(user:any, event:any, paciente:any) {
    this.afs.collection("Agenda").add({
      uid: user.uid,
      displayName: user.displayName, /// mover nombre del doctor
      paciente:paciente,
      idPaciente: paciente.idPaciente,
      evento:event
    })// fin set collection
    .then(() => {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'DATOS GUARDADOS CORRECTAMENTE',
        text: 'SE AGENDO CORRECTAMENTE LA CITA',
        showConfirmButton: false,
        timer: 1500
      })
      /* this.router.navigate(['dashboard']); */
    }).catch((error) => {
      console.error("Error writing document: ", error);
    });
  }

  obtenerAgenda(){
    return this.afs.collection("Agenda").valueChanges();
  }

  obtenerAgenda2(){
    let doctor = JSON.parse(localStorage.getItem('user')!);
    this.afs.collection("cities").ref.where("uid","==", doctor.uid)
    .onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {

        });
    });
  }

  eliminarCita(id:any){
    this.afs.collection("Agenda").doc(id).delete().then(() => {
      console.log("Document successfully deleted!");
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
  }

  actualizarCita(event:any){
    let array = []
    array.push(event)
    this.afs.collection("Agenda").doc(event.uid)
    .update({ evento: array }).then(() => {
      console.log("Document successfully actualizado!");
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
  }

  obtenerAgendaFecha(){
    return this.afs.collection("Agenda").ref.where('evento','==','3 de diciembre de 2021, 11:53:00 UTC-6')
  }
}
