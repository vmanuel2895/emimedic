import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection} from '@angular/fire/firestore';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class FarmaciaService {

  constructor(public afs: AngularFirestore,) { }

  obtenerMedicamentos(){
    return this.afs.collection('Medicamentos').snapshotChanges();
  }

  agregarMedicamentos(user:any, form:any) {
    this.afs.collection("Medicamentos").add({
      uid: user.uid,
      displayName: user.displayName,
      medicamento: form
    }).then(() => {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'DATOS GUARDADOS CORRECTAMENTE',
        text: 'MEDICAMENTO GUARDADO CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
    }).catch((error) => {
      console.error("Error writing document: ", error);
    });
  }

  eliminarMedicamento(id:any){
    this.afs.collection("Medicamentos").doc(id).delete().then(() => {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'SE ELIMINO CORRECTAMENTE EL MEDICAMENTO',
        text: 'MEDICAMENTO ELIMINADO CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
    }).catch((error) => {
        console.error("Error removing document: ", error);
    });
  }
}
