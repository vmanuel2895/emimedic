import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any[] = [
    {
      titulo: 'INICIO',
      icono: 'mdi mdi-home',
      ruta:'/dashboard'
    },
    {
      titulo: 'PACIENTES',
      icono: 'mdi mdi-account-plus',
      ruta:'pacientes'
    },
    {
      titulo: 'COSULTA MÉDICA',
      icono: 'mdi mdi-file-document',
      ruta:'historia/clinica'
    },
    {
      titulo: 'BITÁCORA',
      icono: 'mdi mdi-clipboard-text',
      ruta:'bitacora/pacientes'
    },
    {
      titulo: 'MEDICOS',
      icono: 'mdi mdi-folder-lock-open',
      ruta:'activar/medicos',
      role:'admin'
    },
    /* {
      titulo: 'FARMACIA',
      icono: 'mdi mdi-medical-bag',
      ruta:'medicamentos'
    }, */

  ];

  constructor() { }
}
