import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { finalize, first, map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection} from '@angular/fire/firestore';
import Swal from 'sweetalert2'
import * as firebase from 'firebase/app';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { AgendaService } from '../agenda/agenda.service';
import { CalendarEvent } from 'angular-calendar';
import { startOfDay} from 'date-fns';

export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
  nombre : string;
  apellidoPaterno :string;
  apellidoMaterno: string;
  curp: string;
  telefono: string;
  cedula: string;
  consultorio: string;
  numeroExterior: String;
  colonia:String;
  municipio :String;
  estado :String;
  codigoPostal:String;
  universidad : string;
  genero: string;
  stado: boolean;
  titulo:string;
  fechaRegistro:any;
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  userState: any;
  uploadPercent?: Observable<number|undefined>;
  urlImage?:Observable<string>;
  events: CalendarEvent[] = [];
  public eventos2:any[] = [];
  public day = startOfDay(new Date());

  constructor(public afAuth: AngularFireAuth,
              private router:Router,
              public ngZone: NgZone,
              public afs: AngularFirestore,
              private _storage: AngularFireStorage,
              private _agenda:AgendaService) {
    // funcion para obtener el usuario logueado y setear al usuario en el localstorage
  }

  // funcion para realizar el login
  login(user:string, password:string) {
    firebase.auth().signInWithEmailAndPassword(user, password)
    .then((userCredential) => {
      // Usuario logueado
      this.obtenerDoctor();
      this.afs.collection("Agenda").ref.where("uid","==", userCredential.user?.uid)
      .onSnapshot((querySnapshot) => {
        this.events=[]
        this.eventos2=[];
        querySnapshot.forEach((doc) => {
          doc.data().evento.forEach((elemen:any) => {
            let evento = {
              title: elemen.title,
              start: elemen.start.toDate(),
              end: elemen.end.toDate(),
              color: elemen.color,
              draggable: elemen.draggable,
              resizable: {
                beforeStart: elemen.resizable.beforeStart,
                afterEnd: elemen.resizable.afterEnd,
              },
              uid:doc.id,
              motivo:elemen.motivo,
              consultaUid:doc.data().uid,
              paciente:doc.data().paciente
            }
            this.events = [
              ...this.events,
              evento,
            ];
            if(evento.start.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' }) == this.day.toLocaleDateString('es-MX',{  month: 'long', day: 'numeric' })){
              this.eventos2 = [
                ...this.eventos2,
                evento
              ];
            }
          });
          localStorage.setItem('agenda',JSON.stringify(this.events))
          localStorage.setItem('agenda2',JSON.stringify(this.eventos2))
        });
      });
      this.router.navigate(['dashboard']);
    }).catch((error) => {
      if(error.code == 'auth/wrong-password'){
        // Contraseña incorrecta
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Contraseña incorrecta Comunícate con soporte técnico',
          showConfirmButton: false,
          timer: 1500
        })
      }else if(error.code == 'auth/user-not-found'){
        // Usuario incorrecto
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Correo electronico incorrecto, Comunícate con soporte técnico',
          showConfirmButton: false,
          timer: 1500
        })
      }
    });
  }

  // Funcion para registrar un usuario nuevo
  registrarUser(email:string, password:string,form:any) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then((userCredential) => {
      var user = userCredential.user;
      this.setUserDataRegistro(user,form).then(()=>{
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Usuario registrado correctamente',
          showConfirmButton: false,
          timer: 1500
        })
        this.router.navigate(['login']);
      });
    }).catch((error) => {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Registro invalido',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }

  // Funcion para recuperar la contraseña
  recuperarPassword(passwordResetEmail:string) {
    firebase.auth().sendPasswordResetEmail(passwordResetEmail)
    .then((userCredential) => {
      // Cambio de contraseña por correo
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Cambio de contraseña',
        text: 'Revise su correo electronico!',
        showConfirmButton: false,
        timer: 1500
      })
    }).catch((error) => {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Error',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }

  // Funcion para obtener el usuario logueado
  isLoggedIn(){
    return this.afAuth.authState.pipe(first()).toPromise()
  }

  // Funcion para crear la tabla Doctores y setear al usuario registrado
  setUserDataRegistro(user:any, form:any) {
    var fechas =  Date.now()
    this.actualizarPerfilNombre(form);
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Doctores/${user.uid}`);
    const userState: User = {
      uid: user.uid,
      email: user.email,
      displayName: form.nombre,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      nombre : form.nombre,
      apellidoPaterno: form.apellidoP,
      apellidoMaterno: form.apellidoM,
      curp: form.curp,
      telefono: form.telefono,
      cedula: form.cedula,
      genero:form.genero,
      consultorio:form.consultorio,
      numeroExterior: form.numeroExterior,
      colonia: form.colonia,
      municipio:form.municipio,
      estado:form.estado,
      fechaRegistro: fechas,
      codigoPostal: form.codigoPostal,
      universidad: form.universidad,
      stado: false,
      titulo:form.titulo,
    }
    return userRef.set(userState, {
      merge: true
    });
  }

  //Actualizar el nombre del usaurio registrado
  actualizarPerfilNombre(usuario:any){
    const user = firebase.auth().currentUser;
    user?.updateProfile({
      displayName: usuario.nombre,
    }).then(() => {
    }).catch((error) => {
    });
  }

  //Subir imagen de perfil
  subirImagen(e:any){
    const randomId = Math.random().toString(36).substring(2);
    const file = e;
    const filePath= `imgPerfil/${randomId}`;
    const ref = this._storage.ref(filePath);
    const task = this._storage.upload(filePath, file);
    task.snapshotChanges().pipe(
      finalize(()=>{
        ref.getDownloadURL().subscribe(urlImage => {
          this.urlImage = urlImage;
          this.actualizarImagenPerfil(this.urlImage);
        })
      })
    ).subscribe();
  }

  //Actualizar la imagen en el perfil logueado
  actualizarImagenPerfil(url:any){
    const user = firebase.auth().currentUser;
    user?.updateProfile({
      photoURL: url
    }).then(() => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Doctores/${user.uid}`);
          const userState = {
            photoURL: user.photoURL!,
          }
          userRef.set(userState, {
            merge: true
          });
          this.obtenerDoctor();
        } else {
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Error',
            showConfirmButton: false,
            timer: 1500
          })
        }
      });
    }).catch((error) => {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Error',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }

  //Subir imagen de institucion
  subirImagenInstitucion(e:any,usuario:any){
    const randomId = Math.random().toString(36).substring(2);
    const file = e;
    const filePath= `imgInsti${randomId}`;
    const ref = this._storage.ref(filePath);
    const task = this._storage.upload(filePath, file);
    task.snapshotChanges().pipe(
      finalize(()=>{
        ref.getDownloadURL().subscribe(urlImage => {
          this.urlImage = urlImage;
          this.actualizarImagenInstitucion(this.urlImage,usuario);
        })
      })
    ).subscribe();
  }

  //Actualizar la imagen en el perfil logueado
  async actualizarImagenInstitucion(url:any,usuario:any){
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Doctores/${usuario.uid}`);
    userRef.set({photoIntitucion:url}, { merge: true });
    this.obtenerDoctor();
    /* const addDocument = await docRef.set({ specie: "black cat" });
    await docRef.update({ specie: "Awesome black cat 😺" }); */
  }

  obtenerDoctor(){
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.afs.collection('Doctores').ref.where("uid", "==", user.uid).get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            this.userState = doc.data();
            localStorage.setItem('user', JSON.stringify(this.userState));
          });
        })
      } else {
        /* Swal.fire({
          position: 'top-end',
          icon: 'info',
          title: 'Usuario no encotrado',
          showConfirmButton: false,
          timer: 1500
        }) */
      }
    });
  }

  agregarDatosFacturacion(user:any, form:any) {
    this.afs.collection("Facturacion").add(Object.assign({
      uid: user.uid,
      datos: form
    })).then(()=>{
      this.router.navigate(['dashboard']);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Se guardaron correctamente los datos de facturación',
        showConfirmButton: false,
        timer: 1500
      })
    }).catch((error)=>{
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Error de conexión',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }

  actualizarDatosFacturacion(user:any, form:any, idDatos:any){
    let datos = Object.assign({
      uid: user.uid,
      datos: form
    })
    this.afs.collection("Facturacion").doc(idDatos)
    .update(datos).then(()=>{
      this.router.navigate(['dashboard']);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'DATOS DE FACTURACIÓN ACTUALIZADO CORRECTAMENTE',
        text: 'DATOS DE FACTURACIÓN ACTUALIZADO CON ÉXITO',
        showConfirmButton: false,
        timer: 1500
      })
    }).catch((error) => {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'NO SE ACTUALIZARON LOS DATOS DE FACTURACION',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }

  getFacturacion() {
    return this.afs.collection("Facturacion").snapshotChanges();
  }

  // Funcion para cerrar sesion y borrar al usuario del localstorage
  logout(){
    firebase.auth().signOut()
    .then(() => {
      // logout
      localStorage.clear();
      this.router.navigate(['login']);
    })
    .catch((error) => {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Error de conexión',
        showConfirmButton: false,
        timer: 1500
      })
    });
  }
}
