// import { Injectable } from '@angular/core';
import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { finalize, first, map, take } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection} from '@angular/fire/firestore';
import Swal from 'sweetalert2'
import * as firebase from 'firebase/app';
import { AngularFireStorage } from '@angular/fire/storage';
import { MOMENT } from 'angular-calendar';
import * as moment from 'moment';
import { updateModuleBlock } from 'typescript';
import { HttpClient } from '@angular/common/http';

export interface User {
  uid: string;
  displayName: string;
  nombre : string;
  apellidoPaterno :string;
  apellidoMaterno: string;
  curp: string;
  edad: string;
  sexo:string;
  telefono: string;
  telEmergencia: string;
  calle: string;
  numeroInterior:string;
  numeroExterior:string;
  codigoPostal:string;
  colonia:string;
  municipio:String;
  estado:string;
}

 const fecha =  new Date()
 const fechaClinica = new Date().toDateString();

@Injectable({
  providedIn: 'root'
})

export class PacientesService {

  constructor(public afAuth: AngularFireAuth,
              private router:Router,
              public ngZone: NgZone,
              public afs: AngularFirestore,
              private _storage: AngularFireStorage,
              private http: HttpClient
              // funcion para obtener el usuario logueado y setear al usuario en el localstorage
  ){}

  // funcion para agregar un nuevo paciente
  setUserDataRegistro(user:any, form:any) {
    this.afs.collection("Pacientes").add({
      uid: user.uid,
      displayName: user.displayName, /// mover nombre del doctor
      nombre : form.nombre,
      apellidoPaterno : form.apellidoPaterno,
      apellidoMaterno: form.apellidoMaterno ,
      curp:  form.curp,
      edad:  form.edad,
      sexo: form.sexo,
      telefono: form.telefono,
      telEmergencia: form.telEmergencia,
      calle: form.calle,
      numeroInterior: form.numeroInterior,
      numeroExterior: form.numeroExterior,
      codigoPostal: form.codigoPostal,
      colonia: form.colonia,
      municipio: form.municipio,
      estado: form.estado,
      fechaInicio : fecha,
      activo :true
    })
  }

  // funcion para agregar la historia clinica al paciente
  setHistoriaClinica(user:any, form:any, paciente:any) {
    this.afs.collection("HistoriaClinica").add({
      uid: user.uid,
      displayName: user.displayName, /// mover nombre del doctor
      alergias: form.alergias,
      cie10: form.cie10,
      edad: form.edad,
      paciente:paciente,
      estudios: form.estudios,
      nota: form.nota,
      peso: form.peso,
      ta: form.ta,
      talla: form.talla,
      temp: form.temp,
      imc:form.imc,
      fechaInicio : fechaClinica,
      idPaciente: paciente.idPaciente
    })// fin set collection
    .then(() => {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'DATOS GUARDADOS CORRECTAMENTE',
        text: 'HISTORIA CLINICA GUARDADA CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['dashboard']);
    }).catch((error) => {
      console.error("Error writing document: ", error);
    });
  }

  // funcion para agregar la historia clinica al paciente
  setHistoriaClinicaCompleta(user:any, form:any, paciente:any) {
    this.afs.collection("HistoriaClinicaCompleta").add({
      uid: user.uid,
      displayName: user.displayName, /// mover nombre del doctor
      paciente:paciente,
      fechaInicio : fechaClinica,
      idPaciente: paciente.idPaciente,
      historiaClinica : form
    })// fin set collection
    .then(() => {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'DATOS GUARDADOS CORRECTAMENTE',
        text: 'HISTORIA CLINICA GUARDADA CON EXITO',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigate(['dashboard/pacientes']);
    }).catch((error) => {
      console.error("Error writing document: ", error);
    });
  }

  //funcion para obtener el documento
  obtenerDataPacientes(){
    const datos : any [] = new Array<any>();
    this.afs.collection("Pacientes").valueChanges().subscribe((resultados) =>{
      datos.push (resultados)
      // localStorage.setItem('docPacientes' , JSON.stringify(resultados));
    })
  }
  //fin obtener data pacientes

}

