export function buscar(medicamentos:any, nombre:string){
  let medicamentosEncontrados:any[] = [];
  medicamentos.forEach((element:any) => {
    let medicamento = element.medicamento.nombreComercial.toLocaleLowerCase().trim().replace(/^\s+|\s+$|\s+(?=\s)/g, "");
    if(medicamento.includes(nombre.toLocaleLowerCase().trim().replace(/^\s+|\s+$|\s+(?=\s)/g, ""))){
      medicamentosEncontrados.push(element)
    }
  });
  medicamentosEncontrados.forEach((element:any)=>{
    if(element.medicamento.cantidad == 0){
      Object.assign(element,{existencia:false})
    }else{
      Object.assign(element,{existencia:true})
    }
  })
  return medicamentosEncontrados;
}
