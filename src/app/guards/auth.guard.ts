import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { LoginService } from '../services/login/login.service';
import { AngularFirestore } from '@angular/fire/firestore';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public isLogged = false;
  public user:any;
  public status = false;

  constructor(private _login:LoginService,
              public router: Router,
              public afs: AngularFirestore){}

  async canActivate( route: ActivatedRouteSnapshot,
               state: RouterStateSnapshot){
    this.user = await this._login.isLoggedIn();
    if(this.user != null){
     await this.afs.collection("Doctores").ref.where("uid","==", this.user.uid)
      .get()
      .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            this.status = doc.data().stado;
            if (this.status == true) {
              if(this.user){
                this.isLogged = true;
              }else{
                this.router.navigate(['login'])
              }
            } else {
              this.isLogged = false;
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'Usuario sin permisos',
                showConfirmButton: false,
                timer: 1500
              })
              this.router.navigate(['login'])
              localStorage.clear();
            }
          });
      })
      .catch((error) => {
          console.log("Error getting documents: ", error);
      });
    }else{
      this.isLogged = false;
      this.router.navigate(['login'])
    }
    return this.isLogged;
  }
}
