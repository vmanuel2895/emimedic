// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDtfC--NXjxSChLibtGPlUiBPgmrCajwbo",
    authDomain: "emimedic-4e8cd.firebaseapp.com",
    projectId: "emimedic-4e8cd",
    storageBucket: "emimedic-4e8cd.appspot.com",
    messagingSenderId: "907311646760",
    appId: "1:907311646760:web:5fdf9c725fa898de854d1d",
    measurementId: "G-GQVGKTXG1S"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
